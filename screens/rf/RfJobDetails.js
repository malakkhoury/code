import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Modal,
  TouchableOpacity,
} from "react-native";
import { Button } from "react-native-paper";
import { ListItem } from "react-native-elements";
import * as firebase from "firebase";
import "firebase/firestore";
import { ScrollView } from "react-native-gesture-handler";

const screenWidth = Math.round(Dimensions.get("window").width);

//This screen shows job details of a job listing

export default function RfJobDetails({ route, navigation }) {
  //constants needed for implementation
  const db = firebase.firestore();
  const { data, accepted } = route.params;
  const user_id = global.user_id;
  const [modalVisible, setModalVisible] = useState(false);
  const [ipName, setIpName] = useState("");
  const [ngoName, setNgoName] = useState("");

  //when user accepts job listing, 
  //first check that the job date has not expired
  //if not, remove notification, and update database for notifications, then add user_id to applicant ids in jobs collection in database
  //if job has expired call modalVisible function and set it to true
  const onAccept = () => {
    var jobsRef = db.collection("jobs").doc(data.job_id);
    const exp = parseInt(data.expiryDateBeforeFormat);
    const now = Math.floor(Date.now());

 if(exp < now)
  {
     setModalVisible(true);
      return;
  }
    var difference = jobsRef.expiryDate;
    var removeNotification = jobsRef.update({
      ids: firebase.firestore.FieldValue.arrayRemove(user_id),
    });
    var jobsRef = db.collection("notifications").doc(data.key);
    var removeNotification = jobsRef.update({
      ids: firebase.firestore.FieldValue.arrayRemove(user_id),
    });
    alert("Your application has been sent");

    navigation.navigate("RfNotifications");
    var jobsRef = db.collection("jobs").doc(data.job_id);
    var addApplicant = jobsRef.update({
      applicant_ids: firebase.firestore.FieldValue.arrayUnion(user_id),
    });
    var jobsRef = db.collection("notifications").doc(data.key);
    var addApplicant = jobsRef.update({
      applicant_ids: firebase.firestore.FieldValue.arrayUnion(user_id),
    });
  };

  //if user rejects job remove user_id from ids in job collection in database
  //(ids stores all possible candidates of job and whom the notificaiton should go to for the job listing)
  // delete notification and update notification database 
  //and remove user_id from ids in job collection, then alert user they have declined the job offer listed 
  const onReject = () => {
    var jobsRef = db.collection("jobs").doc(data.job_id);
    var removeNotification = jobsRef.update({
      ids: firebase.firestore.FieldValue.arrayRemove(user_id),
    });
    var jobsRef = db.collection("notifications").doc(data.key);
    var removeNotification = jobsRef.update({
      ids: firebase.firestore.FieldValue.arrayRemove(user_id),
    });
    alert("You Have Declined This Job Offer Listed");
    navigation.navigate("RfNotifications");
  };

  //modalView function to alert user.
  const ModalView = ({ titleText, bodyText, modalVisible }) => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>{titleText}</Text>

            <Text style={styles.textStyle}>{bodyText}</Text>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
              style={styles.button}
            >
              <Text>{"Ok"}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  
  useEffect(() => {
    const subscriber = db
        .collection('implementing_partners')
        .onSnapshot(querySnapshot => {
            querySnapshot.forEach(documentSnapshot => {
              //fetch their name from implementing partner's collection using their id
              //set ip name for job
                if (data.ip_id == documentSnapshot.id) {
                    setIpName(documentSnapshot.data().name);
                }
                //set NGO name to ngo name from collection
                if (data.ip_id == documentSnapshot.id){
                  setNgoName(documentSnapshot.data().ngo_name)
                }
            });
        });
    return () => subscriber();
}, []);

  //display job data details
  return (
    <ScrollView>

<ListItem
    bottomDivider
    containerStyle={{ backgroundColor: 'snow' }} >
    <ListItem.Content>
        <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Implementing partner NGO</ListItem.Title>
        <ListItem.Subtitle>{ngoName}</ListItem.Subtitle>
    </ListItem.Content>
</ListItem>

<ListItem
    bottomDivider
    containerStyle={{ backgroundColor: 'snow' }} >
    <ListItem.Content>
        <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Implementing partner name</ListItem.Title>
        <ListItem.Subtitle>{ipName}</ListItem.Subtitle>
    </ListItem.Content>
</ListItem>

      <View style={{ marginTop: 12, marginBottom: 22 }}>
        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Job Title</ListItem.Title>
            <ListItem.Subtitle>{data.job_title}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>

        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Salary per hour</ListItem.Title>
            <ListItem.Subtitle>{data.salary}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>

        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Location</ListItem.Title>
            <ListItem.Subtitle>{data.location}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>

        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Skill</ListItem.Title>
            <ListItem.Subtitle>{data.skill}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>

        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Job Start Date</ListItem.Title>
            <ListItem.Subtitle>{data.startingDate}</ListItem.Subtitle>

          </ListItem.Content>
        </ListItem>

        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Job End Date</ListItem.Title>
            <ListItem.Subtitle>{data.expiry_date}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>

        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Duration in days</ListItem.Title>
            <ListItem.Subtitle>{data.duration}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>

        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Job Additional Details</ListItem.Title>
            <ListItem.Subtitle>{data.details}</ListItem.Subtitle>
          </ListItem.Content>
        </ListItem>

        {!accepted && (
          <View
            style={{
              flexDirection: "row",
              marginTop: 32,
              justifyContent: "center",
            }}
          >
            <Button
              icon="check"
              color="deepskyblue"
              mode="contained"
              style={{ marginHorizontal: 16, width: screenWidth / 3 }}
              labelStyle={{ fontSize: 18, color: "white" }}
              onPress={onAccept}
            >
              apply
            </Button>
            <Button
              icon="close"
              color="red"
              mode="contained"
              style={{ marginHorizontal: 16, width: screenWidth / 3 }}
              labelStyle={{ fontSize: 18, color: "white" }}
              onPress={onReject}
            >
              ignore
            </Button>
          </View>
        )}
      </View>
      {modalVisible && (
        <ModalView
          titleText={""}
          bodyText={"This job listing has expired"}
          modalVisible={modalVisible}
        />
      )}
    </ScrollView>
  );
}


const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
