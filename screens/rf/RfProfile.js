import React from 'react'
import { View, Text, StyleSheet, TouchableHighlight, ScrollView, Dimensions } from 'react-native'
import { ListItem, Avatar, Rating } from 'react-native-elements'
import { Button } from 'react-native-paper';

//This Screen Shows Refugee's Profile

const data =
{
    profile_url: 'https://www.seekpng.com/png/full/143-1435868_headshot-silhouette-person-placeholder.png',


}

const screenHeight = Math.round(Dimensions.get('window').height);


export default function RfProfile({ navigation }){
    //constants needed for implementation of screen
    //global.user saved from login and used throughout app
    const user = global.user;

        //display user profile 
    //get details from database user collection
    return (
       
            <ScrollView >
                 <View style={styles.container}>
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }} >
    
                    <Avatar size={74} source={{ uri: data.profile_url }} />
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', fontSize:20}}>{user.name}</ListItem.Title>
    
                        <Rating
                            readonly
                            imageSize={20}
                            startingValue={Number(user.rating)}
                            style={{ paddingVertical: 10 }} />
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }} >
                    <ListItem.Content >
                        <ListItem.Title style={{ paddingBottom: 10, fontWeight: 'bold',color: 'steelblue'}}>Age</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.age}</ListItem.Subtitle>
    
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}   >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Gender</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.gender}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}   >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Nationality</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.nationality}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}  >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Skill</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.skill}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}  >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Location</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.location}</ListItem.Subtitle>
    
                    </ListItem.Content>
                </ListItem>
                {
                    //if user clicks on summary, navigate to RfSummary
                }
                <ListItem
                    bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}
                    onPress={() =>
                        navigation.navigate("RfSummary",
                            {
                                summary: user.summary,
                            })
                    } >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Summary</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{`${user.summary.substring(0, 32)}...`}</ListItem.Subtitle>
                    </ListItem.Content>
                    <ListItem.Chevron color="steelblue"/>
                </ListItem>
                
                {
                    //if user clicks on request for edits, navigate to RfProfileEdit screen
                }
    
               
                   
                   
                   
                     <Button
                        color="steelblue"
                        mode="outlined"
                        style={{ marginBottom: 8, marginHorizontal: 8, marginTop:15, borderRadius:15, borderWidth: 2, borderColor:'steelblue' }}
                        labelStyle={{ fontSize: 18, }}
                        onPress={() =>
                            navigation.navigate("RfProfileEdit",
                                {
                                    data: user,
                                })
                        } >
                        Request edits in profile
                       
                        </Button>
              
                
                </View>
            </ScrollView>
        )
    }
    const styles = StyleSheet.create({
        container: {
            flex: 1,
            paddingVertical: 8,
            paddingHorizontal: 2,
            height: screenHeight*1.1
        },
    
        requestEditText: {
            paddingBottom: 4,
            textAlign: 'center',
            color: 'steelblue',
            textDecorationLine: 'underline',
            fontSize: 20,
            fontWeight: "bold"
        }
    });