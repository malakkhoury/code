import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';
import { ScrollView } from 'react-native-gesture-handler';

//This screen shows job details of an ongoing job for refugees

export default function RfCurrentJob() {
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const [jobs, setJobs] = useState([])
    const user_id = global.user_id;
    const [ipName, setIpName] = useState("");
    const [ngoName, setNgoName] = useState("");

    //This function fetches jobs that are currently ongoing
    useEffect(() => {
        const subscriber = db
            .collection('jobs')
            .onSnapshot(querySnapshot => {
                //jobs array to store currently ongoing jobs from database
                const jobs = [];
                querySnapshot.forEach(documentSnapshot => {
                    if (documentSnapshot.data().accepted_ids != undefined) {
                        if (documentSnapshot.data().accepted_ids.length > 0
                        ) {
                            //Calculate the time difference from when job was created to job duration
                             //If difference is <= duration of job duration, then the job is displayed
                             //only if includes the user_id (global.user_id)
                             //push jobs into jobs array
                            if (documentSnapshot.data().accepted_ids.includes(user_id)) {
                                let time1 = new Date().getTime();
                                let time2 = documentSnapshot.data().date;
                                let difference = time1 - time2;
                                var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
                                if (daysDifference>0 && daysDifference < documentSnapshot.data().duration) {
                                jobs.push({
                                    ...documentSnapshot.data(),
                                    key: documentSnapshot.id,
                                });

                                }
                            }
                        }
                    }
                });
                setJobs(jobs);
            });
        // Unsubscribe from events when no longer in use
        return () => subscriber();
    }, []);

        //get ip name to display in job details
        if(jobs.length > 0 && jobs[0] != undefined)
        {
            firebase.firestore()
               .collection('implementing_partners')
               .onSnapshot(querySnapshot => {
                   querySnapshot.forEach(documentSnapshot => {
                       if ( jobs[0].ip_id == documentSnapshot.id) {
                           setIpName(documentSnapshot.data().name);
                       }
                       if( jobs[0].ip_id == documentSnapshot.id){
                           setNgoName(documentSnapshot.data().name);
                       }
                   });
               });
          
        }
        
  


    //display details 
    return (
        <View style={styles.container}>
            <ScrollView>
            <Text style={styles.headerText}>CURRENT JOB</Text>
            <Divider style={{ backgroundColor: 'deepskyblue' }} />
            {
                jobs.length > 0 && jobs[0] != undefined ?
                    <View>
                        <ListItem
    bottomDivider
    containerStyle={{ backgroundColor: 'snow' }} >
    <ListItem.Content>
        <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Implementing partner NGO</ListItem.Title>
        <ListItem.Subtitle>{ngoName}</ListItem.Subtitle>
    </ListItem.Content>
</ListItem>
                         <ListItem bottomDivider
                            containerStyle={{ backgroundColor: 'snow' }} >
                            <ListItem.Content>
                                <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Implementing Partner</ListItem.Title>
                                <ListItem.Subtitle>{ipName}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>
                        <ListItem bottomDivider
                            containerStyle={{ backgroundColor: 'snow' }} >
                            <ListItem.Content>
                            <ListItem.Title style={ {color: 'steelblue', fontSize: 18}}>Job Title</ListItem.Title>
                            <ListItem.Subtitle></ListItem.Subtitle>
                                <ListItem.Subtitle style={ {fontSize: 18}}>{jobs[0].job_title}</ListItem.Subtitle>
                                <ListItem.Subtitle></ListItem.Subtitle>
                                <ListItem.Subtitle>{jobs[0].details}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>

                        <ListItem bottomDivider
                            containerStyle={{ backgroundColor: 'snow' }} >
                            <ListItem.Content>
                                <ListItem.Title>Salary</ListItem.Title>
                                <ListItem.Subtitle>{`$${jobs[0].salary} / Hour`}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>

                        <ListItem bottomDivider
                            containerStyle={{ backgroundColor: 'snow' }} >
                            <ListItem.Content>
                                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Skills</ListItem.Title>
                                <ListItem.Subtitle>{jobs[0].skill}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>

                        <ListItem bottomDivider
                            containerStyle={{ backgroundColor: 'snow' }} >
                            <ListItem.Content>
                                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Location</ListItem.Title>
                                <ListItem.Subtitle>{jobs[0].location}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>

                        <ListItem bottomDivider
                            containerStyle={{ backgroundColor: 'snow' }} >
                            <ListItem.Content>
                                <ListItem.Title>Duration</ListItem.Title>
                                <ListItem.Subtitle>{`${jobs[0].duration} days`}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>


                        <ListItem bottomDivider
                            containerStyle={{ backgroundColor: 'snow' }} >
                            <ListItem.Content>
                                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Starting Date</ListItem.Title>
                                <ListItem.Subtitle>{`${jobs[0].startingDate}`}</ListItem.Subtitle>
                            </ListItem.Content>
                        </ListItem>
                        <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Ending Date </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle>{`${jobs[0].expiry_date}`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

                    </View>

                    : <Text style={styles.emptyMsg}>You are currently not in recruitement.</Text>
            }
</ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    },
    emptyMsg: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
    }

});