import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';

//This screen shows previously done jobs for refugee user
export default function RfPreviousJobs({ navigation }) {
    const db = firebase.firestore();
    const [loading, setLoading] = useState(true);
    const [jobs, setJobs] = useState([]);
    const user_id = global.user_id;

    //created jobs array.
    //if job has accepted ids, (so job had users assigned to it) and job's accepted_ids includes user_id (refugee) 
    //then calculate job time and finish time, if job has ended,
    //then push job onto jobs array. (which will be displayed)
    useEffect(() => {
        const subscriber = db
            .collection('jobs')
            .onSnapshot(querySnapshot => {
                const jobs = [];
                querySnapshot.forEach(documentSnapshot => {
                    if (documentSnapshot.data().accepted_ids != undefined) {
                        if (documentSnapshot.data().accepted_ids.includes(user_id)

                        ) {
                            let current_time = new Date().getTime();
                            let starting_time = documentSnapshot.data().date;
                            let difference = current_time - starting_time;
                            var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
                            if (daysDifference > documentSnapshot.data().duration) {
                                jobs.push({
                                    ...documentSnapshot.data(),
                                    key: documentSnapshot.id,
                                });
                            }
                        }
                    }
                });
                setJobs(jobs);
                setLoading(false);
            });
        return () => subscriber();
    }, []);

    //key extractor for flatlist unique key
    const keyExtractor = (item, index) => index.toString();
        //on user pressing on job, navigate to "RfPreviousJobsDetails" screen with job item
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            onPress={() =>
                navigation.navigate("RfPreviousJobsDetails",
                    {
                        job: item,
                    }
                )
            }>
            <ListItem.Content>
                <ListItem.Title>{item.job_title}</ListItem.Title>
                <ListItem.Subtitle>{item.details.toString().substring(0, 30) + "..."}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem>
    );

    return (
        //display previous jobs title
        <View style={styles.container}>
            <Text style={styles.headerText}>PREVIOUS JOBS</Text>
            <Divider style={{ backgroundColor: 'deepskyblue' }} />

            {
                jobs.length > 0 ?
                    <FlatList
                        style={{ marginBottom: 50 }}
                        keyExtractor={keyExtractor}
                        data={jobs}
                        renderItem={renderItem} />
                    : <Text style={styles.emptyMsg}>Completed Jobs will be shown here</Text>
            }

        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    },
    emptyMsg: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
    }

});