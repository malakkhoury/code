import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  View,
  ActivityIndicator,
} from "react-native";

import { SwipeListView } from "react-native-swipe-list-view";
import * as firebase from "firebase";
import "firebase/firestore";
  
//This screen displays a list of notifications to the IP

export default function RfNotifications({ navigation }) {
  //constants needed for implementation
  const db = firebase.firestore();
  const [loading, setLoading] = useState(true);
  const [notifications, setNotifications] = useState([]);
  const user_id = global.user_id;

  //close row
  const closeRow = (rowMap, rowKey) => {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow();
    }
  };

  //delete row
  const deleteRow = (rowMap, rowKey) => {
    closeRow(rowMap, rowKey);
    const newData = [...notifications];
    const prevIndex = notifications.findIndex((item) => item.key === rowKey);
    newData.splice(prevIndex, 1);
    setNotifications(newData);
  };

  const onRowDidOpen = (rowKey) => {
    console.log("this row opened");
  };


  const renderItem = (data) => (
    <TouchableHighlight
    //when user clicks on notification, if notification title == New Job Listing set accepted == true
    //(accepted == false in rfJobDetails means that user still hasn't accepted the job so in RfJobDetails, show accept or decline buttons)
      onPress={() => {
        let notificationRef = db.collection("notifications").doc();

        if (data.item.notification_title == "New Job Listing") {
          navigation.navigate("RfJobDetails", {
            data: data.item,
            accepted: false,
          });
        } else {
          navigation.navigate("RfJobDetails", {
            data: data.item,
            accepted: true,
          });
        }
      }}
      style={styles.rowFront}
      underlayColor={"ghostwhite"}
    >
    {
      //show notification title and job title
    }
      <View>
      <Text style={{fontSize:19, paddingBottom: 30, paddingTop: 10}}>
          {data.item.notification_title}: {data.item.job_title}
        </Text>
      </View>
    </TouchableHighlight>
  );

  //delete row == delete notificaiton id from notificaiton collection in database 
  //and job id from jobs collection in database
  const renderHiddenItem = (data, rowMap) => (
    <View style={styles.rowBack}>
      <TouchableOpacity
        style={[styles.backRightBtn, styles.backRightBtnRight]}
        onPress={() => {
          deleteRow(rowMap, data.item.key);
          let doc_id = data.item.key.substring(0, 20);
          var jobsRef = db.collection("notifications").doc(doc_id);
          if (data.item.notification_title == "New Job Listing") {
            var removeNotification = jobsRef.update({
              ids: firebase.firestore.FieldValue.arrayRemove(user_id),
            });
          } else {
            var removeNotification = jobsRef.update({
              accepted_ids: firebase.firestore.FieldValue.arrayRemove(user_id),
            });
          }
        }}
      >
        <Text style={styles.backTextWhite}>Delete</Text>
      </TouchableOpacity>
    </View>
  );
  //when user clicks on notification, set global.hasnotificaitons = false so that notification alert doesn;t remain red. (IpHome)
  useEffect(() => {
    let i = 0;
    global.hasNotifications = false;

    //the functions fetches if user has notiicaitons, if user_id is in ids then have a notification title of New Job Listing 
  //(ids stores ids of possible applicants for job)
  //if user_id is in accepted_ids, then htis means that the IP has accepted them so have a notification title of job accepted. 
  //add notifications to notificaitons array
    const subscriber = firebase
      .firestore()
      .collection("notifications")
      .onSnapshot((querySnapshot) => {
        const notifications = [];

        querySnapshot.forEach((documentSnapshot) => {
          if (documentSnapshot.data() != undefined) {
            if (documentSnapshot.data().ids.includes(user_id)) {
              notifications.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
                notification_title: "New Job Listing",
              });
            }
            if (documentSnapshot.data().accepted_ids != undefined) {
              if (documentSnapshot.data().accepted_ids.includes(user_id)) {
                notifications.push({
                  ...documentSnapshot.data(),
                  key: documentSnapshot.id + i,
                  notification_title: "Job Accepted"
                });
                i += 1;
              }
            }
          }
        });
        setNotifications(notifications);
        setLoading(false);
      });

    return () => subscriber();
  }, []);

  if (loading) {
    return <ActivityIndicator />;
  }
  //if notificaitons array is empty, then user has no notiifcaitons so print no new notificiations
  //else, display notifications
  return notifications.length === 0 ? (
    <Text style={styles.emptyMsg}>No New Notifications</Text>
  ) : (
    <View style={styles.container}>
      <SwipeListView
       data={notifications}
        showsVerticalScrollIndicator={false}
        renderItem={renderItem}
        renderHiddenItem={renderHiddenItem}
        leftOpenValue={0}
        rightOpenValue={-75}
        previewRowKey={"0"}
        previewOpenValue={-40}
        previewOpenDelay={3000}
        onRowDidOpen={onRowDidOpen}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  backTextWhite: {
    color: "white",
  },
  rowFront: {
    alignItems: "center",
    backgroundColor: "white",
    borderBottomColor: "gainsboro",
    borderBottomWidth: 2,
    justifyContent: "center",
    height: 70,
  },
  rowBack: {
    alignItems: "center",
    backgroundColor: "ghostwhite",
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 15,
  },
  backRightBtn: {
    alignItems: "center",
    bottom: 0,
    justifyContent: "center",
    position: "absolute",
    top: 0,
    width: 75,
  },
  backRightBtnRight: {
    backgroundColor: "red",
    right: 0,
    borderRadius: 1,
  },

  emptyMsg: {
    flex: 1,
    textAlign: "center",
    textAlignVertical: "center",
    fontWeight: "bold",
    fontSize: 18,
  },
});