import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, SafeAreaView, Dimensions } from 'react-native'
import { Button } from 'react-native-paper';
import { ScrollView } from 'react-native-gesture-handler';
import * as firebase from 'firebase';
import 'firebase/firestore';

//this screen updates the summary of the user

export default function RfSummary({ route, navigation }) {
     //constants needed for implementation of screen
    const db = firebase.firestore();
    const user_id = global.user_id;
    const { summary } = route.params;
    const [text, setText] = useState("");
    const screenWidth = Math.round(Dimensions.get('window').width);

    const updateSummary = () => {
        //summary can't be empty
        if (text == "") { 
            alert("Please enter new text for summary")
        } else {
            //update user summary to what they enter
            //then navigate to RfHome
            var docRef = db.collection("refugees").doc(user_id);
             docRef.update({
                 //text stores the user input and summary in database is updated to text value
                summary: text
            })
                .then(() => {
                    global.user.summary = text;
                    alert("Summary updated!");
                    navigation.navigate("Profile",
                    {'paramPropKey': 'paramPropValue'});
                    return
                })
                .catch((error) => {
                    console.error("Error updating document: ", error);
                });
        }
    }
    return (
        //display
        <SafeAreaView style={styles.container}>
        <ScrollView>
            <View>

                <Text style={styles.headerText}>Describe yourself in a summary for others to see: </Text>
                {
                    //takes in what the user typed and stores it into text to display
                }
                <TextInput
                    style={styles.summaryText}
                    multiline={true}
                    placeholder="Say something about yourself"
                    defaultValue={summary}
                    onChangeText={text => setText(text)}
                />
                <View style={{ flexDirection: 'row', marginTop: 16, justifyContent: 'center' }} >
                    {
                        //submit button
                    }
                    <Button
                        color="deepskyblue"
                        mode="contained"
                        style={{ marginHorizontal: 16, width: screenWidth / 3 }}
                        labelStyle={{ fontSize: 18, color: "white" }}
                        onPress={updateSummary}>
                        submit
                </Button>
                    {
                        //cancel button
                    }
                    <Button
                        color="red"
                        mode="contained"
                        style={{ marginHorizontal: 16, width: screenWidth / 3 }}
                        labelStyle={{ fontSize: 18, color: "white" }}
                        onPress={() => navigation.navigate("RfHome")}>
                        cancel
                </Button>
                </View>

            </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingHorizontal: 8,
    },
    headerText: {
        fontSize: 18,
        marginTop: 32,
        marginBottom: 4,

    },
    summaryText: {
        textAlignVertical: 'top',
        height: 120,
        backgroundColor: "#d7e0e3",
        fontSize: 16,
        padding: 8,
        color: "black",

        borderRadius: 4,
    },

    registerText: {
        textAlign: 'center',
        marginTop: 32,
        color: 'deepskyblue'
    }
});