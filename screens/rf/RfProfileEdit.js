import React, { useState } from 'react';
import {
    View, Text, TextInput, StyleSheet, Dimensions, KeyboardAvoidingView,
    ScrollView,
} from 'react-native'
import { Button } from 'react-native-paper';
import UploadDocument from '../../components/UploadDocument'
import { ListItem } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';


const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

//This screen shows refugee a screen from where they can submit an edit profile request
export default function RfProfileEdit({ route, navigation }) {
    //constants needed for implementation
    const { data } = route.params;
    const db = firebase.firestore();
    const user_id = global.user_id;
    const user = global.user;
    const [details, setDetails] = useState("");

    //A complaint is added to the complaints collection in firebase
    const onRequestChange = () => {
        //if empty string alert
        if (
            details == ""
        ) {
            alert("Please fill in the form before submitting.");
        }
        else {
            //else, add request to complaints collection
            db.collection("complaints").add({
                refugee_id: user_id,
                details: details,
                refugee_name: user.name,
            })
                .then((docRef) => {
                    alert("Your request has been submitted");
                    navigation.goBack();
                })
                .catch((error) => {
                    console.error("Error adding document: ", error);
                });
        }
    }

    //display
    return (
        <ScrollView
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}>
            <KeyboardAvoidingView enabled >
            <View style={styles.container}>
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }} >
                    <ListItem.Content >
                        <ListItem.Title style={{ paddingBottom: 10, color: 'steelblue', fontWeight: 'bold'}}>Name</ListItem.Title>
                        <ListItem.Subtitle  style={{ fontSize: 20}}>{data.name}</ListItem.Subtitle>
                    </ListItem.Content>
                 </ListItem>

                    <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }} >
                    <ListItem.Content >
                        <ListItem.Title style={{ paddingBottom: 10, fontWeight: 'bold', color: 'steelblue'}}>Age</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.age}</ListItem.Subtitle>
    
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}   >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Gender</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.gender}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}   >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Nationality</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.nationality}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}  >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Skill</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.skill}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
    
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}  >
                    <ListItem.Content>
                        <ListItem.Title style={{ paddingBottom: 10,  fontWeight: 'bold', color:'steelblue'}}>Location</ListItem.Title>
                        <ListItem.Subtitle style={{ fontSize: 20}}>{user.location}</ListItem.Subtitle>
    
                    </ListItem.Content>
                </ListItem>
    

                    <TextInput
                        style={styles.inputBox}
                        multiline={true}
                        placeholder="Please state clearly what information you would like changed."
                        onChangeText={value => setDetails(value)}
                    />

                    <UploadDocument />

                    <Button
                        color="deepskyblue"
                        mode="contained"
                        style={{ marginBottom: 50 }}
                        labelStyle={{ fontSize: 18, color: "white" }}
                        onPress={onRequestChange}>
                        REQUEST CHANGE
                    </Button>

                </View>
            </KeyboardAvoidingView>
        </ScrollView >
    )
}

const styles = StyleSheet.create({

    container: {
        marginHorizontal: 8,
      height: screenHeight * 1.5,
       width: screenWidth 
    },
    headerText: {
        fontSize: 18,
        marginTop: 8,
        marginBottom: 8,
    },
    infoText: {
        marginHorizontal: screenWidth / 4
    },
    inputBox: {
        textAlignVertical: 'top',
        height: 120,
        backgroundColor: '#d7e0e3',
        fontSize: 16,
        padding: 8,
        marginTop: 32,
        borderRadius: 4,
    },

    registerText: {
        textAlign: 'center',
        marginTop: 32,
        color: 'deepskyblue'
    }
});

