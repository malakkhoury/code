import React, { useState, useEffect} from 'react';
import { View, StyleSheet, ScrollView  } from 'react-native'
import { ListItem } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';

// This screen shows the DETAILS of a queued Job
export default function RfQueuedJobDetails({ route, navigation }) {
    const db = firebase.firestore();
    const { job } = route.params;
    const [ipName, setIpName] = useState("");
    const [ngoName, setNgoName] = useState("");

    useEffect(() => {
        const subscriber = db
            .collection('implementing_partners')
            .onSnapshot(querySnapshot => {
                querySnapshot.forEach(documentSnapshot => {
                  //fetch their name from implementing partner's collection using their id
                  //set ip name for job
                    if (job.ip_id == documentSnapshot.id) {
                        setIpName(documentSnapshot.data().name);
                    }
                    //set NGO name to ngo name from collection
                    if (job.ip_id == documentSnapshot.id){
                      setNgoName(documentSnapshot.data().ngo_name)
                    }
                });
            });
        return () => subscriber();
    }, []);

    return (

         //display data details
        <View style={styles.container}>
             <ScrollView>
             <ListItem
    bottomDivider
    containerStyle={{ backgroundColor: 'snow' }} >
    <ListItem.Content>
        <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Implementing partner NGO</ListItem.Title>
        <ListItem.Subtitle>{ngoName}</ListItem.Subtitle>
    </ListItem.Content>
</ListItem>
            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Implementing Partner Name</ListItem.Title>
                    <ListItem.Subtitle>{ipName}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}> Job Title</ListItem.Title>
                    <ListItem.Title >{job.job_title}</ListItem.Title>
                    <ListItem.Subtitle>{job.details}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Location</ListItem.Title>
                    <ListItem.Subtitle>{job.location}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Salary</ListItem.Title>
                    <ListItem.Subtitle>{`${job.salary} / Hour`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Skill</ListItem.Title>
                    <ListItem.Subtitle>{job.skill}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Duration</ListItem.Title>
                    <ListItem.Subtitle>{`${job.duration} days`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>


            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Starting Date</ListItem.Title>
                    <ListItem.Subtitle>{`${job.startingDate}`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Ending Date </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle>{job.expiry_date}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            </ScrollView>
        </View>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 8,
        paddingHorizontal: 2,

    },

    requestEditText: {
        padding: 4,
        textAlign: 'center',
        color: 'deepskyblue',
        textDecorationLine: 'underline',
    },
    applicantText: {
        marginTop: 8,
        marginLeft: 4,
        padding: 4,
        fontSize: 18,
    }
});

