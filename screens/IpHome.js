import * as React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Icon } from "react-native-elements";
import { useEffect } from "react";
import * as firebase from "firebase";
import "firebase/firestore";

import IpProfileScreen from "./ip/IpProfile";
import IpPreviousJobsScreen from "./ip/IpPreviousJobs";
import IpCurrentJobsScreen from "./ip/IpCurrentJobs";
import IpQueuedJobsScreen from "./ip/IpQueuedJobs";
import IpCreateJobScreen from "./ip/IpCreateJob";
import _ from "lodash";

//logout signout from firebase and navigate to landing page (login page)
function Logout({ navigation }) {
  const user_id = global.user_id;
  useEffect(() => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        navigation.replace("Landing");
      })
      .catch((error) => alert(error.message));
  }, []);
  return <View></View>;
}

const Drawer = createDrawerNavigator();

//get locations of refugees to add to global.locations for creating jobs 
async function fetchLocation() {
  await firebase
    .firestore()
    .collection("refugees")
    .onSnapshot((data) => {
      const locations = [];
      data.forEach((user) => {
        locations.push({
          label: user.data().location,
          value: user.data().location,
        });

      });
      //only one of each location should be saved
      global.locations = _.uniqBy(locations, "label");
    });

}
//get skills of refugees to add to global.locations for creating jobs 
async function fetchSkills() {
  await firebase
    .firestore()
    .collection("refugees")
    .onSnapshot((data) => {
      const skills = [];
      data.forEach((user) => {
        skills.push({
          label: user.data().skill,
          value: user.data().skill,
        });

      });
      //only one of each skill should be saved
      global.skills = _.uniqBy(skills, "label");
    });
}

export default function IpHome({ navigation }) {
  
  useEffect(() => {
    //get locations and skills for create job
    fetchLocation();
    fetchSkills();

    //check if user has notifications  from firebase, if ip_id == user id then user has notificaitons
    const subscriber =
     firebase
      .firestore()
      .collection("notifications")
      .onSnapshot((querySnapshot) => {
        global.hasNotifications = false;
        querySnapshot.forEach((documentSnapshot) => {
          if (documentSnapshot.data() != undefined) {
            if (documentSnapshot.data().applicant_ids != undefined) {
              if (
                documentSnapshot.data().applicant_ids.length > 0 &&
                documentSnapshot.data().ip_id == user_id
              ) {
                  global.hasNotifications = true;
              }              
              //messaging and notification icons and navigation
              //if user has notifications set bell-alert icon to red 
                
                navigation.setOptions({
                  headerRight: () => (
                    <View style={{ flexDirection: "row" }}>
                      <TouchableOpacity
                        style={{ marginRight: 8 }}
                        onPress={() => navigation.navigate("IpNotifications")}
                      >
                        
                        { global.hasNotifications ?  (
                          <Icon
                            type="material-community"
                            name="bell-alert"
                            size={32}
                            color="red"
                          />
                        ) : (
                          <Icon
                            type="material"
                            name="notifications"
                            size={32}
                            color="#D3D3D3"
                          />
                        )}
                      </TouchableOpacity>
                  
                      <TouchableOpacity
                        style={{ marginRight: 8 }}
                        onPress={() => navigation.navigate("ChatList")}
                      >
                        <Icon
                          type="material"
                          name="email"
                          size={32}
                          color="#D3D3D3"
                        />
                      </TouchableOpacity>
                    </View>
                  ),
                });
                return;
              }
            }
          
        });
      });

    return () => subscriber();
  }, []);

  //drawers for naivgation.
  return (
    <Drawer.Navigator initialRouteName="IpProfile">
      <Drawer.Screen name="Profile" component={IpProfileScreen} />
      <Drawer.Screen name="Create Job" component={IpCreateJobScreen} />
      <Drawer.Screen name="Current Date Jobs" component={IpCurrentJobsScreen} />
      <Drawer.Screen name="Queued Dates Jobs" component={IpQueuedJobsScreen} />
      <Drawer.Screen name="Jobs to be Rated" component={IpPreviousJobsScreen} />
      <Drawer.Screen name="Logout" component={Logout} />
    </Drawer.Navigator>
  );
}