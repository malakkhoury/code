import React, {useState} from 'react';
import {
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView,
    View,
    Dimensions,
    Text
} from 'react-native';
import { Input } from 'react-native-elements';
import { Button } from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import * as firebase from 'firebase';
import 'firebase/firestore';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
//This screen allows user to register an account
//constants needed for implementation of screen
export default function Register({ navigation }){
    const [user, setUser] = useState('unhcr');
    const [name, setName] = useState(0);
    const [reg_id, setRegId] = useState(0);
    const [password, setPassword] = useState(0);
    const [re_password, setRePassword] = useState(0);
    const [underLineColor0, setUnderLineColor0] = useState('grey');
    const [underLineColor1, setUnderLineColor1] = useState('grey');
    const [underLineColor2, setUnderLineColor2] = useState('grey');
    const [underLineColor3, setUnderLineColor3] = useState('grey');
    const [underLineColor4, setUnderLineColor4] = useState('grey');
    const db = firebase.firestore();

   //Function to handle signup
    const onSignUp = () => {
        //empty field validation
        if (name == "" | reg_id == "" || password == "" || re_password == "") {
            alert("One or more fields are empty")
        }

        else {
            //passwords don't mathc validation
            if (password != re_password) {
                alert("Passwords Don't Match");
            }
            else {
            //Check if registration ID and name exist in database
            //check if entered registration ID exists in firebase database
            //user is what the user set as their type. below i fetch for the collection with the usertype the user set and 
            //compare reg_id with the reg_id they entered, if it doesn't exist then an alert shows saying that the user doesn't exist.
            //if it does exist then we set global.user to this user object, global.user_id to the user id the collection is saved as, 
            //and user type to type the user chose when logging in
                db.collection(user).where("reg_id", "==", reg_id).where("name", "==", name)
                    .get()
                    .then((querySnapshot) => {
                        if (querySnapshot.empty) {
                      //show error if name and registration id do not exist
                            alert("Registration ID or Name does not exist. You have to be a registered UN staff, Refugee or Implementing Partner");
                        }
                        else {
                            firebase
                                .auth()
                                .createUserWithEmailAndPassword(reg_id + "@" + user.replace("_", "") + ".com", password)
                                .then((res) => {
                                    alert("User Registered Successfully");
                                    //Go back to login screen
                                    navigation.goBack();
                                })
                                .catch(error => alert('User already exists'))
                        }

                    })
                    .catch((error) => {
                        console.log("Error getting documents: ", error);
                    });
            }
        }
    }

    return (
        <KeyboardAvoidingView 
        behavior="padding"
        >
        <ScrollView>
                <View style={styles.container}>
                <DropDownPicker 
                    items={[
                        {label: 'UNHCR Staff', value: 'unhcr'},
                        {label: 'Refugee', value: 'refugees' },
                        {label: 'Implementing Partner', value: 'implementing_partners'},
                    ]}
                    placeholder = "Choose a user option"
                    containerStyle={{height: 50, flexDirection: "center", width: '75%', marginHorizontal:50, fontWeight:'bold'}}
                    style={{
                        borderTopLeftRadius: 10, borderTopRightRadius: 10,
                        borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                    }}
                    itemStyle={{
                       borderBottomColor: 'black',

                    }}
                    labelStyle={{
                        textAlign: 'center',
                        color: '#000',
                    
                    }}
                    selectedLabelStyle={{
                        color: '#39739d',
                        fontSize: 20,
             
                    }}

                    globalTextStyle={{
                        fontSize: 15
                    }}
                    dropDownStyle={{
                        borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                    }}
                    onChangeItem={item => setUser(item.value)}
                />
                <Text></Text>
                <Text></Text>
                <Text></Text>
                <Text></Text>

                    <Input 
                        onFocus={() => setUnderLineColor1('steelblue')}
                        onBlur={() => setUnderLineColor1('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor1 }}

                        label="Full Name"
                        placeholder="First and Last Name"
                        leftIcon={{ type: 'material', name: 'face', color: 'grey' }}
                        onChangeText={value => setName(value)}
                    />
                    <Input
                        onFocus={() => setUnderLineColor2('steelblue')}
                        onBlur={() => setUnderLineColor2('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor2 }}
                        label="Your Registration ID"
                        placeholder="Registration Card (ID)"
                        leftIcon={{ type: 'material', name: 'tag', color: 'grey' }}
                        keyboardType="numeric"
                        onChangeText={value => setRegId(value)}
                    />
                    <Input
                        onFocus={() => setUnderLineColor3('steelblue')}
                        onBlur={() => setUnderLineColor3('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor3 }}
                        label="Password"
                        placeholder="Password"
                        leftIcon={{ type: 'material', name: 'lock', color: 'grey' }}
                        secureTextEntry={true}
                        onChangeText={value => setPassword(value)}
                    />

                    <Input
                        onFocus={() => setUnderLineColor4('steelblue')}
                        onBlur={() => setUnderLineColor4('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor4 }}
                        label="Confirm Password"
                        placeholder="Confirm Password"
                        leftIcon={{ type: 'material', name: 'lock', color: 'grey' }}
                        secureTextEntry={true}
                        onChangeText={value => setRePassword(value)}
                    />

                    <Button
                        color="steelblue"
                        mode="contained"
                        style={{ marginVertical: 8, marginHorizontal: 12 }}
                        labelStyle={{ fontSize: 18, color: "white" }}
                        onPress={() => onSignUp()}>
                        REGISTER
                    </Button>

                </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 12,
        paddingTop: 16,
        paddingBottom: 32,
        width: screenWidth,
        height: screenHeight
    },
 
    userSelectionTile: {
        paddingLeft: 10,
        margin: 4,
    },
});
