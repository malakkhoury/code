import React, { useState } from 'react';
import {
    StyleSheet,
    View,
    TouchableHighlight,
    StatusBar,
    Dimensions,
    KeyboardAvoidingView,
    ScrollView,
    Text
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { Input } from 'react-native-elements';
import { Button } from 'react-native-paper';
import * as firebase from 'firebase';
import 'firebase/firestore';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

//This is the main landing page. It allows user to sign in or go to the 
//registeration page. 
export default function Landing({ navigation }) {
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const [user, setUser] = useState('unhcr');
    const [isScanned, setIsscanned] = useState(false);

    const [underLineColor1, setUnderLineColor1] = useState('grey');
    const [underLineColor2, setUnderLineColor2] = useState('grey');
    const [reg_id, setRegId] = useState('');
    const [password, setPassword] = useState('');
  
//This function executes when user clicks on Login button 
//check for any empty fields
    const onLogin = () => {
        if (
            reg_id == "" || password == ""
        ) {
            alert("One or more fields are empty.")
        } else {
     //check if entered registration ID exists in firebase database
     //user is what the user set as their type. below i fetch for the collection with the usertype the user set and 
     //compare reg_id with the reg_id they entered, if it doesn't exist then an alert shows saying that the user doesn't exist.
     //if it does exist then we set global.user to this user object, global.user_id to the user id the collection is saved as, 
     //and user type to type the user chose when logging in
            db.collection(user).where("reg_id", "==", reg_id)
                .get()
                .then((querySnapshot) => {
                    if (querySnapshot.empty) {
                        alert("Registration ID does not exist (please check you have chosen the correct user type and the Reqisteration ID is correct)");
                    }
                    else {
                        global.user = querySnapshot.docs[0].data();
                        console.log("user:", global.user);
                        global.user_id = querySnapshot.docs[0].id;
                        console.log("user ID :", global.user_id);
                        global.user_type = user;
                        console.log("user Type:", global.user_type);

                        //treating the registeration id as an email to be able to store it in authentication in the firebase. 
                        //Assigning registeration numbers as the first part of their email (name) and their user types as their email web app name.
                        
                        const email= reg_id + "@" + user.replace("_", "") + ".com";
                        firebase
                            .auth()
                            .signInWithEmailAndPassword(email, password)
                            .then((res) => {
                                //navigate each user to their home page. 
                                if (user == "unhcr") {

                                    navigation.replace("UnHome")
                                }
                                else if (user == "implementing_partners") {
                                    navigation.replace("IpHome" ,)
                                }
                                else if (user == "refugees") {
                                    navigation.replace("RfHome")
                                }
                            })
                            .catch(error => alert("Incorrect Password or user is not registered"))

                    }

                })
                .catch((error) => {
                    console.log("Error getting documents: ", error);
                });
        }
    }

    return (
        //style 
        <ScrollView>
            <KeyboardAvoidingView 
            > 
             <View style={styles.header}> 
                 <Text style={styles.headerTitle}>UNOpp</Text>
                 </View>
            <View style={styles.container}>
                    <Text style={styles.headerText} style={{color: 'grey',   textAlign: 'center',
       fontWeight: 'bold',
        fontSize: 20,
        paddingTop: StatusBar.currentHeight + 10,
        marginBottom: 32,
        marginTop: 20,
        
        }}>LOGIN</Text>

                   
                    <DropDownPicker 
                    items={[
                        {label: 'UNHCR Staff', value: 'unhcr'},
                        {label: 'Refugee', value: 'refugees' },
                        {label: 'Implementing Partner', value: 'implementing_partners'},
                    ]}
                    placeholder="Choose a user option"
                    containerStyle={{height: 50, flexDirection: "row", width: '75%', marginHorizontal:50}}
                    style={{
                        borderTopLeftRadius: 10, borderTopRightRadius: 10,
                        borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                    }}
                   
                    labelStyle={{
                        textAlign: 'center',
                        color: '#000'
                    }}
                    selectedLabelStyle={{
                        color: 'steelblue',
                        fontSize: 18
                    }}

                    globalTextStyle={{
                        fontSize: 15
                    }}
                    dropDownStyle={{
                        borderBottomLeftRadius: 20, borderBottomRightRadius: 20, 
                    }}
                    onChangeItem={item => setUser(item.value)}
                />
                <Text>
                </Text>
                <Text>  
                </Text>
                    <Input 
                        onFocus={() => setUnderLineColor1('steelblue')}
                        onBlur={() => setUnderLineColor1('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor1 }}
                        label="Your Registration ID"
                        placeholder="Registeration Card Number"
                        leftIcon={{ type: 'material', name: 'tag', color: 'grey' }}
                        keyboardType="numeric"
                        onChangeText={value => setRegId(value)}
                    />

                    <Input
                        onFocus={() => setUnderLineColor2('steelblue')}
                        onBlur={() => setUnderLineColor2('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor2 }}
                        label="Password"
                        placeholder="Password"
                        leftIcon={{ type: 'material', name: 'lock', color: 'grey' }}
                        secureTextEntry={true}
                        onChangeText={value => setPassword(value)}
                    />

                    <Button
                        icon="qrcode-scan"
                        color="black"
                        mode="text"
                        style={{ marginTop: 8 }}
                        labelStyle={{ fontSize: 18, }}
                        onPress={() => {
                            setIsscanned(true);
                            navigation.navigate("ScanId");
                        }}>
                        <Text style={{textDecorationLine: 'underline'}}>Scan QR code from ID</Text> 
                    </Button>
                        <Text>

                        </Text>
                    <Button 
                        color="steelblue"
                        mode="contained"
                        style={{ marginTop: 16, paddingEnd: 30}}
                        labelStyle={{ fontSize: 20, color: "white" }}
                        onPress={() => isScanned ? onLogin() : alert('Please Scan your ID')}
                        style={{textDecorationLine: 'underline' }}>
                        <Text style={{width: screenWidth/10}} >LOGIN </Text>
                    </Button>
                    
                    <Text style={styles.infoText}>Don't have an account?</Text>
                    <TouchableHighlight
                        activeOpacity={0.7}
                        underlayColor="#cccccc"
                        style={{ borderRadius: 2 }}
                        onPress={() => navigation.navigate("Register")} >
                        <Text style={styles.registerText}>Register here</Text>
                    </TouchableHighlight>
                </View>
            </KeyboardAvoidingView>
        </ScrollView>
    )
}
const styles = StyleSheet.create({

    header: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: screenHeight*0.15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'steelblue'
       
      },
      headerTitle: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold',
        paddingTop: 29
      }, 
    container: {
        flex: 1,
        paddingHorizontal: 20,
        backgroundColor: '#fff',
        height: screenHeight
    },
    choiceContainer: {
        backgroundColor: "#fff",
        elevation: 4,
        borderRadius: 8,
        marginBottom: 32,

    },
    userSelectionTile: {
        paddingLeft: 10,
        margin: 4,
    },
    infoText: {
        textAlign: 'center',
        marginHorizontal: 10,
        fontSize: 14,
        marginTop: 16,
        height: 20
    },

    registerText: {
        textAlign: 'center',
        fontSize: 18,
        marginVertical: 8,
        color: 'steelblue'
    },
    dropDown: {
        paddingTop: 60,
       
        width: '20%'
    }
});