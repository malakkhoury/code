import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';

//this screen shows the Ip's queued jobs 

export default function IpQueuedJobs({ navigation }) {
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const [loading, setLoading] = useState(true);
    const [jobs, setJobs] = useState([]);
    const user_id = global.user_id;

     //fetch from jobs collection in database, after render.
     //calculate timings of jobs in database if the current time minus the starting time
    //of the job, is less than 0, then the job date is still to come, so push it to jobs array to later display as queued job 
    useEffect(() => {
        const subscriber = firebase.firestore()
            .collection('jobs')
            .onSnapshot(querySnapshot => {
                const jobs = [];
                querySnapshot.forEach(documentSnapshot => {
                    if (documentSnapshot.data().ip_id == user_id
                    ) {
                        let current_time = new Date().getTime();
                        let starting_time = documentSnapshot.data().date;
                        let difference = current_time - starting_time;
                        if (difference < 0) {
                            jobs.push({
                                ...documentSnapshot.data(),
                                key: documentSnapshot.id,
                            });
                        }
                    }
                });
                setJobs(jobs);
                setLoading(false);
            });
        return () => subscriber();
    }, []);

    //key extractor for flatlist unique key
    const keyExtractor = (item, index) => index.toString();

    //on user pressing on job, navigate to IpQueuedJobDetails screen with job item
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            onPress={() =>
                navigation.navigate("IpQueuedJobDetails",
                    {
                        allowRating: false,
                        job: item,
                    }
                )}
        >

         {
            //display job title 
         }
            <ListItem.Content>
                <ListItem.Title>{item.job_title}</ListItem.Title>
                <ListItem.Subtitle>{item.details.toString().substring(0, 30) + "..."}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem>
    );

    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>QUEUED DATES JOBS</Text>

           { //if there are any jobs being stored in jobs array then display them, else say "no jobs in queue"
}
            <Divider style={{ backgroundColor: 'deepskyblue' }} />
            {
                jobs.length > 0 ?
                    <FlatList
                        style={{ marginBottom: 50 }}
                        keyExtractor={keyExtractor}
                        data={jobs}
                        renderItem={renderItem} />
                    :
                        <View>
                         <Text  style={styles.emptyMsg}>No Jobs in queue {"\n"} </Text>
                         <Text tyle={styles.emptyMsgTwo}>(jobs in queue means that the date of the job hasn't come yet)</Text>
                        </View>
            }
        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    },
    emptyMsg: {
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
        paddingTop: '60%'
    },
    emptyMsgTwo: {
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 10,
        paddingTop: '60%',
        borderRightWidth: 30
        
    }

});