//This screen shows refugee's profile as applicants, (from notificaitons)
import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Dimensions, ScrollView } from 'react-native'
import { ListItem, Avatar, Rating } from 'react-native-elements'
import { Button } from 'react-native-paper';
import * as firebase from 'firebase';
import 'firebase/firestore';
const screenWidth = Math.round(Dimensions.get('window').width);

//profile image avatar
const data =
{
    profile_url: 'https://www.seekpng.com/png/full/143-1435868_headshot-silhouette-person-placeholder.png',

}

export default function IpApplicantProfile({ route, navigation }) {
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const { job, job_notification_id, applicant, applicant_id} = route.params;
    const [is_available, setIsAvailable] = useState(true);
    const user_id = global.user_id;
   
    //check and compare user is not already assigned to a job within the same time frame as new job and if so set isAvailable to false
    useEffect(() => {
        db.collection("jobs").get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                if (doc.data().accepted_ids != undefined) {
                    if (doc.data().accepted_ids.includes(applicant_id)) {
                        const first_job_date = parseInt(doc.data().date);
                        const first_job_durationinHours = parseInt(doc.data().duration * 86400000);
                        const first_job_end_date = first_job_date + first_job_durationinHours;
                        const second_job_date = job.date;
                        const second_job_end_Date = job.expiryDateBeforeFormat;
                        if(second_job_date <= first_job_end_date && first_job_date <= second_job_end_Date)
                        {
                              setIsAvailable(false);
                             return; 
                        }
                    }
                }
            });
        });
    }, []);

    //if the applicant is available when IP user accepts applicant, update jobs and notifications in database
    const onAccept = () => {  
        if(is_available){
            var jobsRef = db.collection('jobs').doc(job.job_id);
            var removeNotification = jobsRef.update({
                applicant_ids: firebase.firestore.FieldValue.arrayRemove(applicant_id)
            });
            var jobsRef = db.collection('notifications').doc(job_notification_id);
            var removeNotification = jobsRef.update({
                applicant_ids: firebase.firestore.FieldValue.arrayRemove(applicant_id)
            });
            var jobsRef = db.collection('jobs').doc(job.job_id);
            var addApplicant = jobsRef.update({
                accepted_ids: firebase.firestore.FieldValue.arrayUnion(applicant_id)
            });
            var jobsRef = db.collection('notifications').doc(job_notification_id);
            var addApplicant = jobsRef.update({
                accepted_ids: firebase.firestore.FieldValue.arrayUnion(applicant_id)
            });
            
            alert("This Applicant Has Been Accepted");
        }
        //if available == false, alert applicant is not available
        else{
            alert("Sorry, your request could not be submitted. This Applicant is NOT available at the moment");
            db.collection('jobs').doc(job.job_id).update({
            applicant_ids: firebase.firestore.FieldValue.arrayRemove(applicant_id)
            });
            db.collection('notifications').doc(job_notification_id).update({
                applicant_ids: firebase.firestore.FieldValue.arrayRemove(applicant_id)
            });
        }
        
        navigation.goBack();
    }

    //if user rejects applicant update notifications and jobs collections in database 
    const onReject = () => {

        var jobsRef = db.collection('jobs').doc(job.job_id);
        var removeNotification = jobsRef.update({
            applicant_ids: firebase.firestore.FieldValue.arrayRemove(applicant_id)
        });

        var jobsRef = db.collection('notifications').doc(job_notification_id);
        var removeNotification = jobsRef.update({
            applicant_ids: firebase.firestore.FieldValue.arrayRemove(applicant_id)
        });

        alert("This Applicant Has Been Rejected");
        navigation.goBack();
    }

    //view applicant profile
    return (
        <ScrollView>
            <View style={styles.container}>
                <ListItem
                    containerStyle={{ backgroundColor: 'snow' }} >

                    <Avatar size={74} source={{ uri: data.profile_url }} />
                    <ListItem.Content>
                        <ListItem.Title>{applicant.name}</ListItem.Title>

                        <Rating
                            readonly
                            imageSize={20}
                            startingValue={Number(applicant.rating)}
                            style={{ paddingVertical: 8 }} />
                    </ListItem.Content>
                </ListItem>

                <View style={styles.buttonsContainer} >

                    <Button
                        icon="account"
                        color="black"
                        mode="outlined"
                        style={{ marginBottom: 8, marginHorizontal: 8 }}
                        labelStyle={{ fontSize: 18, }}
                        onPress={() => alert('Interview has been scheduled')}>
                        INTERVIEW
                </Button>

                    <Button
                        icon="email"
                        color="black"
                        mode="outlined"
                        style={{ marginBottom: 8, marginHorizontal: 8 }}
                        labelStyle={{ fontSize: 18, }}
                        onPress={() => navigation.navigate("Chat", {
                            user_id: user_id,
                            recipient_id: applicant_id,
                        })}>
                        MESSAGE
                </Button>
                </View>
                 <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
                   <ListItem.Content>
                        <ListItem.Title style={{color: 'steelblue', fontSize: 20}}>Applied for</ListItem.Title>
                        <Text></Text>
                        <ListItem.Subtitle style={{fontSize: 20}} >{job.job_title}</ListItem.Subtitle>
                    </ListItem.Content>
                    </ListItem>
                    <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Job Starting Date</ListItem.Title>
            <ListItem.Subtitle>{job.startingDate}</ListItem.Subtitle>

          </ListItem.Content>
        </ListItem>
        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Job Ending Date</ListItem.Title>
            <ListItem.Subtitle>{job.expiry_date}</ListItem.Subtitle>

          </ListItem.Content>
        </ListItem>
        <ListItem containerStyle={{ backgroundColor: "snow" }} bottomDivider>
          <ListItem.Content>
            <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Job Location</ListItem.Title>
            <ListItem.Subtitle>{job.location}</ListItem.Subtitle>

          </ListItem.Content>
        </ListItem>

                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }} >
                    <ListItem.Content>
                        <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Applicant Age</ListItem.Title>
                        <ListItem.Subtitle>{applicant.age}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>

                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}   >
                    <ListItem.Content>
                        <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Applicant Gender</ListItem.Title>
                        <ListItem.Subtitle>{applicant.gender}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>

                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}   >
                    <ListItem.Content>
                        <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Applicant Nationality</ListItem.Title>
                        <ListItem.Subtitle>{applicant.nationality}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>

                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}  >
                    <ListItem.Content>
                        <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Applicant Skill</ListItem.Title>
                        <ListItem.Subtitle>{applicant.skill}</ListItem.Subtitle>

                    </ListItem.Content>

                </ListItem>
                <ListItem bottomDivider
                    containerStyle={{ backgroundColor: 'snow' }}  >
                    <ListItem.Content>
                        <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Applicant Camp Location</ListItem.Title>
                        <ListItem.Subtitle>{applicant.location}</ListItem.Subtitle>

                    </ListItem.Content>

                </ListItem>
                <ListItem
                    bottomDivider
                    onPres
                    containerStyle={{ backgroundColor: 'snow' }}>
                    <ListItem.Content>
                        <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Summary</ListItem.Title>
                        <ListItem.Subtitle>{applicant.summary}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>

                <View style={{ flexDirection: 'row', marginTop: 16, justifyContent: 'center' }} >
                    <Button
                        icon="check"
                        color="deepskyblue"
                        mode="contained"
                        style={{ marginHorizontal: 16, width: screenWidth / 3 }}
                        labelStyle={{ fontSize: 18, color: "white" }}
                        onPress={onAccept}>
                        accept
                </Button>

                    <Button
                        icon="close"
                        color="red"
                        mode="contained"
                        style={{ marginHorizontal: 16, width: screenWidth / 3 }}
                        labelStyle={{ fontSize: 18, color: "white" }}
                        onPress={onReject}>
                        decline
                </Button>
                </View>

            </View>
        </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 8,
        paddingHorizontal: 2,
    },

    requestEditText: {
        padding: 4,
        textAlign: 'center',
        color: 'deepskyblue',
        textDecorationLine: 'underline',
    },

    buttonsContainer: {
        flexDirection: 'row',
        marginTop: 8,
        paddingBottom: 8,
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#bcbbc1',
    }
});