import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, ScrollView, LogBox } from 'react-native'
import { ListItem } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';

//This screen shows job details of an ongoing job for implementing partners
export default function IpCurrentJobDetails({ route, navigation }) {

//ignore scrollview warning
 useEffect(() => {
    LogBox.ignoreLogs(["VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead."]);
 }, [])
 
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const { job } = route.params;
    const [workers, setWorkers] = useState([]);
//key extractor for flatlist unique key
    const keyExtractor = (item, index) => index.toString();
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            onPress={() => {
                var docRef = db.collection("refugees").doc(item.key);
                docRef.get().then((doc) => {
                    //if document exists and allow rating == true, navigate to IpRatingProfile for user to rate performance
                    //else navigate to IpAcceptedProfile page.
                    if (doc.exists) {
                        navigation.navigate("IpAcceptedProfile",
                            {
                                job: job,
                                applicant_id: doc.id,
                                applicant: doc.data(),
                            }
                        )
                    }
                }).catch((error) => {
                    console.log("Error getting document:", error);
                });
            }
            }
        >

            <ListItem.Content>
                <ListItem.Title>{item.name}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem >
    );
    //This function fetches users from database that are currently working in this job
    //and stores thme in 'workers' array
    useEffect(() => {
        const subscriber = firebase.firestore()
            .collection('refugees')
            .onSnapshot(querySnapshot => {
                const workers = [];

                //push accepted ids who are not in rated ids (rated ids means the job has ended) to workers array
                querySnapshot.forEach(documentSnapshot => {
                    if (job.accepted_ids != undefined && job.accepted_ids.length > 0) {
                        if (job.accepted_ids.includes(documentSnapshot.id)
                        &&!(job.rated_ids.includes(documentSnapshot.id))) {
                            workers.push({
                                key: documentSnapshot.id,
                                name: documentSnapshot.data().name,
                            });
                        }
                    }
                });
                setWorkers(workers);
            });
            // Unsubscribe from events when no longer in use
            return () => subscriber();
           
    }, []);

    return (
//display job details from database 
        <View style={styles.container}>
           <ScrollView>
            
        <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    {//if workers length == 0 that means that there are is no one currently assigned to the job so "Job is not in progress"
                    //else, job is in progress
                    }
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Status</ListItem.Title>
                    <ListItem.Subtitle>{workers.length==0? <Text style={{color: 'red'}}> Job not in progress </Text> :  <Text style={{color: 'green'}}>Job accepted by both ends</Text>}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>


            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>{job.job_title}</ListItem.Title>
                    <ListItem.Subtitle>{job.details}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Location</ListItem.Title>
                    <ListItem.Subtitle>{job.location}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Salary</ListItem.Title>
                    <ListItem.Subtitle>{`${job.salary} / Hour`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Skill</ListItem.Title>
                    <ListItem.Subtitle>{job.skill}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Duration</ListItem.Title>
                    <ListItem.Subtitle>{`${job.duration} days`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>


            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Starting Date</ListItem.Title>
                    <ListItem.Subtitle>{`${job.startingDate}`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Ending Date </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle>{job.expiry_date}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>



            <Text style={styles.applicantText}>On-board Refugees</Text>

            {
                workers.length == 0 ?
                <Text style={{paddingLeft: 12, paddingTop: 12, fontSize: 16}}>No refugees on-board</Text> :
                    <FlatList
                        style={{ marginBottom: 50 }}
                        keyExtractor={keyExtractor}
                        data={workers}
                        renderItem={renderItem}
                    />
            }
        
            
    </ScrollView>
        </View>


    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 8,
        paddingHorizontal: 2,

    },

    requestEditText: {
        padding: 4,
        textAlign: 'center',
        color: 'deepskyblue',
        textDecorationLine: 'underline',
    },
    applicantText: {
        marginTop: 8,
        marginLeft: 4,
        padding: 4,
        fontSize: 18,
        color: 'steelblue'
    }
});

