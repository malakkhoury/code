import React, { useState } from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import { ListItem, Avatar, Rating } from 'react-native-elements'
import { Button } from 'react-native-paper';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { LogBox } from 'react-native';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

//This screen shows job details of user awaiting feedback ratingjob
LogBox.ignoreLogs(['VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead.']);

const user =
{
    profile_url: 'https://www.seekpng.com/png/full/143-1435868_headshot-silhouette-person-placeholder.png',


}
export default function IpRatingProfile({ route, navigation }) {
     //constants needed for implementation of screen
    const [rating, setRating] = useState(0);
    const db = firebase.firestore();
    const { job,applicant_id, applicant } = route.params;

    const onRate = () => {
        var docRef = db.collection("refugees").doc(applicant_id);
        docRef.get().then((doc) => {
            if (doc.exists) {
                let previous_rating = doc.data().rating;
                //get average of ratings
                let new_rating = (Number(rating) + Number(previous_rating)) / 2;
                //update rating
                return docRef.update({
                    rating: previous_rating == 0 ? rating : new_rating,
                })
                    .then(() => {
                        var jobsRef = db.collection('jobs').doc(job.key);
                        var addRatedId = jobsRef.update({
                            rated_ids: firebase.firestore.FieldValue.arrayUnion(applicant_id)
                        });
                        alert("Rating Complete");
                        navigation.goBack();
                       // navigation.goBack();
                    })
                    .catch((error) => {
                        console.error("Error updating document: ", error);
                    });
            }
        }).catch((error) => {
            console.log("Error getting document:", error);
        });
    }
    //set rating value
    const ratingCompleted = rating => {
        setRating(rating)
    }

    //display data
    return (
        <View style={styles.container}>
            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <Avatar size={74} source={{ uri: user.profile_url }} />
                <ListItem.Content>
                    <ListItem.Title>{applicant.name}</ListItem.Title>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title>Age</ListItem.Title>
                    <ListItem.Subtitle>{applicant.age}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}   >
                <ListItem.Content>
                    <ListItem.Title>Gender</ListItem.Title>
                    <ListItem.Subtitle>{applicant.gender}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}   >
                <ListItem.Content>
                    <ListItem.Title>Skill</ListItem.Title>
                    <ListItem.Subtitle>{applicant.skill}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <Rating
                showRating
                fractions={1}
                imageSize={32}
                startingValue={rating}
                onFinishRating={ratingCompleted}
                style={{ paddingBottom: 16, marginTop: 16, }}

            />

            <Button
                color="deepskyblue"
                mode="contained"
                style={{ marginVertical: 32, marginHorizontal: 12 }}
                labelStyle={{ fontSize: 18, color: "white" }}
                onPress={onRate}>
                publish rating
            </Button>

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 8,
        paddingHorizontal: 2,
        width: screenWidth,
        height: screenHeight
    },

    requestEditText: {
        padding: 4,
        textAlign: 'center',
        color: 'deepskyblue',
        textDecorationLine: 'underline',
    },

    buttonsContainer: {
        flexDirection: 'row',
        marginTop: 8,
        paddingBottom: 8,
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#bcbbc1',
    }
});

