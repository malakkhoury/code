import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';

//This screen shows jobs that have finished (jobs to be rated)

export default function IpPreviousJobs({ navigation }) {
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const [loading, setLoading] = useState(true);
    const [jobs, setJobs] = useState([]);
    const user_id = global.user_id;

    //if job has accepted ids, (so job had users assigned to it) and job's ip_id is the same as the user's id,
    //and the job's rated ids are not equal to the accepted ids (if they are equal then job shouldn't be displayed
    //as then no more ids would need rating). If all is true, get time of job period, if duration of job has passed
    //then push job onto jobs array. (which will be displayed)
    useEffect(() => {
        const subscriber = firebase.firestore()
            .collection('jobs')
            .onSnapshot(querySnapshot => {
                const jobs = [];
                querySnapshot.forEach(documentSnapshot => {
                    if (documentSnapshot.data().accepted_ids != undefined) {
                        if (documentSnapshot.data().accepted_ids.length > 0
                            && documentSnapshot.data().ip_id == user_id
                            && documentSnapshot.data().rated_ids.length != 
                            documentSnapshot.data().accepted_ids.length
                        ) {
                            let current_time = new Date().getTime();
                            let starting_time = documentSnapshot.data().date;
                            let difference = current_time - starting_time;
                            var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
                            if (daysDifference > documentSnapshot.data().duration) {
                                jobs.push({
                                    ...documentSnapshot.data(),
                                    key: documentSnapshot.id,
                                });
                            }
                        }
                    }
                });
                setJobs(jobs);
                setLoading(false);
            });

        return () => subscriber();
    }, []);
    //key extractor for flatlist unique key
    const keyExtractor = (item, index) => index.toString();
    const renderItem = ({ item }) => (
        //when user clicks on a job to be rated, navigate to IpRatingJobs screen, set allow rating to true (so that when in IpRating Jobs user can rate job because it is a previous on), and job to item user clicked on
        //this will only happen if accepted id > 1.
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            onPress={() =>
                navigation.navigate("IpRatingJobs",
                    {
                        job: item,
                    }
                )
            }>   
    {
    //job titles
}
            <ListItem.Content>
                <ListItem.Title>{item.job_title}</ListItem.Title>
                <ListItem.Subtitle>{item.details.toString().substring(0, 30) + "..."}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem>
    );

    return (
        //diplay of page, if jobs.length == 0, then there are no jobs to be rated
                <View style={styles.container}>
            <Text style={styles.headerText}>JOBS TO BE RATED</Text>
            <Divider style={{ backgroundColor: 'deepskyblue' }} />
            {
                jobs.length > 0 ?
                    <FlatList
                        style={{ marginBottom: 50 }}
                        keyExtractor={keyExtractor}
                        data={jobs}
                        renderItem={renderItem} />
                    : <Text style={styles.emptyMsg}>Completed Jobs to be rated will be shown here</Text>
            }
           
        </View>
    )
}
const styles = StyleSheet.create({

    container: {

        flex: 1,
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    },
    emptyMsg: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
    }

});