import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';

//this screen shows ongoing jobs 

export default function IpCurrentJobs({ navigation }) {
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const [loading, setLoading] = useState(true);
    //array to store ongoing jobs
    const [jobs, setJobs] = useState([]);
    const user_id = global.user_id;

//after rendering fetch jobs that are currently ongoing 
    useEffect(() => {
        const subscriber = firebase.firestore()
            .collection('jobs')
            .onSnapshot(querySnapshot => {
                //jobs array to store currently ongoing jobs from database
                const jobs = [];
                querySnapshot.forEach(documentSnapshot => {
                    //only if includes the user_id (global.user_id)
                    if (documentSnapshot.data().ip_id == user_id
                    ) {
                   //Calculate the time difference from when job was created to job duration
                   //If difference is <= duration of job duration, then the job is displayed
                   //push jobs into jobs array
                        let current_time = new Date().getTime();
                        let starting_time = documentSnapshot.data().date;
                        let difference = current_time - starting_time;
                        let days_difference = Math.floor(difference / 1000 / 60 / 60 / 24);
                        if (days_difference > 0 && days_difference < documentSnapshot.data().duration) {
                            jobs.push({
                                ...documentSnapshot.data(),
                                key: documentSnapshot.id,
                            });
                        }
                    }
                });
                setJobs(jobs);
                setLoading(false);
            });
        // Unsubscribe from events when no longer in use
        return () => subscriber();
    }, []);

    //key extractor for flatlist unique item
    const keyExtractor = (item, index) => index.toString();
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            //when user presses on job, navigate to job details page, with specific job details (item)
            onPress={() =>
                navigation.navigate("IpCurrentJobDetails",
                    {
                        job: item,
                    }
                )}
        >
        {
            //display job titles
        }
            <ListItem.Content>
                <ListItem.Title>{item.job_title}</ListItem.Title>
                <ListItem.Subtitle>{item.details.toString().substring(0, 30) + "..."}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem>
    );
    
    return (
        <View style={styles.container}>
          
            {//show jobs, but if jobs array is empty, then say no on going jobs
}
            <Text style={styles.headerText}>CURRENT DATES JOBS</Text>

            <Divider style={{ backgroundColor: 'deepskyblue' }} />
            {
                jobs.length > 0 ?
                    <FlatList
                        style={{ marginBottom: 50 }}
                        keyExtractor={keyExtractor}
                        data={jobs}
                        renderItem={renderItem} />

                    : <Text style={styles.emptyMsg}>No Ongoing Jobs</Text>
            }

        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    },
    emptyMsg: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
    }
});