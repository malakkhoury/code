import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { ListItem } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';

//This screen shows applicant requests for listed jobs

export default function IpRequests({ navigation }) {
    console.log('-----------------')
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const user_id = global.user_id;
    const [loading, setLoading] = useState(true); 
    const [applications, setApplications] = useState([]); 
    //keyExtractor for flatlist unique key
    const keyExtractor = (item, index) => index.toString();

    //on rendering and user pressing navigate to IpApplicantProfile
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            onPress={() => {
                var docRef = db.collection("refugees").doc(item.refugee_id);
                docRef.get().then((doc) => {
                    if (doc.exists) {
                        navigation.navigate("IpApplicantProfile",
                            {
                                job: item,
                                job_notification_id:item.job_notification_id,
                                applicant: doc.data(),
                                applicant_id: doc.id
                            }
                        )
                    }
                }).catch((error) => {
                    console.log("Error getting document:", error);
                });
            }}
        >
            <ListItem.Content>
                <ListItem.Title>{item.job_title}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem>
    );
    //when page runs, fetch job applicants data
    useEffect(() => {
        const subscriber = firebase.firestore()
            .collection('jobs')
            .onSnapshot(querySnapshot => {
                //array to store all applicant names
                const applications = [];

                querySnapshot.forEach(documentSnapshot => {
                    if (documentSnapshot.data().applicant_ids != undefined
                    ) {
                        if (documentSnapshot.data().applicant_ids.length > 0
                            && documentSnapshot.data().ip_id == user_id
                        ) {
                            documentSnapshot.data().applicant_ids.forEach(myFunction);

                            function myFunction(item, index) {
                                applications.push({
                                    ...documentSnapshot.data(),
                                    job_notification_id: documentSnapshot.job_notification_id,
                                    refugee_id: item,
                                });
                            }
                        }
                    }
                });
                setApplications(applications);
                setLoading(false);
            });
        return () => subscriber();
    }, []);

    //display 
    return (
        <View style={styles.container}>
            <Text style={styles.headerText}>PENDING APPLICATIONS</Text>
            {
                applications.length == 0 ?
                    <Text style={{ textAlign: 'center', paddingTop: 32, fontSize: 16 }}>No pending applications</Text> :
                    <FlatList
                        style={{ marginBottom: 50 }}
                        keyExtractor={keyExtractor}
                        data={applications}
                        renderItem={renderItem}
                    />
            }
        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    }

});