import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  TextInput,
  TouchableHighlight,
  Modal,
} from 'react-native';
import { Button } from 'react-native-paper';
import { Input } from 'react-native-elements';
import * as firebase from 'firebase';
import 'firebase/firestore';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import { TouchableOpacity } from 'react-native';
import { ActivityIndicator } from 'react-native';
import moment from 'moment';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

//This screen allows IP to create a job
export default function IpCreatejob({ navigation }) {
  const db = firebase.firestore();
  const [job_title, setJobTitle] = useState('');
  const [salary, setSalary] = useState('');
  const [location, setLocation] = useState('');
  const [skill, setSkill] = useState('');
  const [details, setDetails] = useState('');
  const [duration, setDuration] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [isVisible, setIsVisible] = useState(true);
  const [underLineColor1, setUnderLineColor1] = useState('grey');
  const [underLineColor2, setUnderLineColor2] = useState('grey');
  const [underLineColor3, setUnderLineColor3] = useState('grey');
  const ids = [];
  const ip_id = global.user_id;

  const [date, setDate] = useState(new Date().getTime());
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    //when user changes date don't show date picker anymore
    setShow(false);
  
    //convert time to midnight of the day, so that when the date comes, the job is sent to current date job
    const letCurrentDate = new Date(currentDate).getTime();
    let dateInstance = new Date(letCurrentDate);
    let x = dateInstance.setHours(0,0,0,0);
    const convert = new Date(x).getTime();
    setDate(convert);
  };

  //function for when user presses on Job Starting Date (below) then show date picker
  const showDatepicker = () => {
    setShow(true);
  };

  //This function is executed when IP creates a Job
  const onPublish = () => {
    //variables needed to store in job collection for validation of creating jobs with
    //previous dates and for user accepting jobs that have been expired. 
    const durationInHours = parseInt(duration * 86400000); 
    const expiryDateBeforeFormat = date + durationInHours;
    const dateFormatted = moment(date).format('LL');
    const expiryDate = moment(expiryDateBeforeFormat).format('LL');
    const nowsDate = Math.floor(Date. now());
 
  //previous date
   if (date < nowsDate){
       alert("The date of the job you create cannot be on or before today's date");
       return;
   }
   //Empty field validation
  
    if (
      job_title == '' ||
      salary == '' ||
      location == '' ||
      skill == '' ||
      duration == '' 
    ) {
      alert('One or more fields are empty');
      return;
    }
    //salary is written in letters validation
    if(
      /[a-z]/i.test(salary)
    )
    {
      alert('Salary should be in numbers only');
      return;
    }
    //duration is only in letters validation
    if(!/\d/.test(duration))
    {
        alert('duration should include numbers');
        return;
    }
    //job title is not in letters
    if(
      /\d/.test(job_title)
    )
    {
      alert('Job Title should be in letters only');
      return;
    }

    //Check if there are refugees matching the location and skill of the posted job
    else {
      db.collection('refugees')
        .where('skill', '==', skill)
        .get()
        .then((querySnapshot) => {
          //If there are no refugees, alert user.
          if (querySnapshot.empty) {
            alert(
              'Currently there are no refugees within your selected criteria'
            );
          } else {
            let flag = false;

            //If there are refugees matching the condition,store their ids.
            querySnapshot.forEach((doc) => {
              if (doc.data().location == location && doc.data().skill == skill) {
                ids.push(doc.id);
                flag = true;
              }
            });

            if (flag) {
              //Add job to firebase database along with saved ids of refugees and the ip user's id
              //update jobs collection and notifications collection
              let jobRef = db.collection('jobs').doc();
              let notificationRef = db.collection('notifications').doc();

              db.collection('jobs')
                .doc(jobRef.id)
                .set({
                  job_id: jobRef.id,
                  job_notification_id: notificationRef.id,
                  job_title: job_title,
                  ids: ids,
                  applicant_ids: [],
                  accepted_ids: [],
                  rated_ids: [],
                  ip_id: ip_id,
                  salary: salary,
                  details:
                    details == '' ? 'No additional details provided' : details,
                  duration: duration,
                  location: location,
                  skill: skill,
                  startingDate: dateFormatted,
                  creationDate: moment().format(),
                  expiry_date: expiryDate,
                  expiryDateBeforeFormat: String(expiryDateBeforeFormat),
                  date: String(date)
                })
                .then((docRef) => {
                  //modal to state that job has been posted (function below) 
                  setModalVisible(true);
                })
                .catch((error) => {
                  console.error('Error adding document: ', error);
                });

              db.collection('notifications')
                .doc(notificationRef.id)
                .set({
                  job_id: jobRef.id,
                  job_title: job_title,
                  ids: ids,
                  applicant_ids: [],
                  accepted_ids: [],
                  rated_ids: [],
                  ip_id: ip_id,
                  salary: salary,
                  details:
                    details == '' ? 'No additional details provided' : details,
                  duration: duration,
                  location: location,
                  skill: skill,
                  startingDate: dateFormatted,
                  creationDate: moment().format(),
                  expiry_date: expiryDate,
                  expiryDateBeforeFormat: String(expiryDateBeforeFormat),
                  date: String(date)
                })

                .catch((error) => {
                  console.error('Error adding document: ', error);
                });
              //Send notification to refugees matching job description
            } else {
              //if 
              alert(
                'Currently there are no refugees within your selected criteria'
              );
            }
          }
        })
        .catch((error) => {
          console.log('Error getting documents: ', error);
        });
    }
  };

  if (loading) {
    return <ActivityIndicator />;
  }

  //modal to tell user job is created
  const ModalView = ({ titleText, bodyText, modalVisible }) => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>{titleText}</Text>

            <Text style={styles.textStyle}>{bodyText}</Text>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(!modalVisible);
                navigation.navigate('Profile');
              }}
              style={styles.button}
            >
              <Text style={{ color: 'blue' }}>{'Ok'}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

//display
  return (
    <ScrollView>
      {!loading && (
        <>
          <View style={styles.container}>
            <DropDownPicker
              zIndex={5000}
              onOpen={() => setIsVisible(false)}
              onClose={() => setIsVisible(true)}
              placeholder="Choose a location"
              //retrieved the location from the refugees database (from IpHome)
              items={global.locations}
              arrowColor="steelblue"
              containerStyle={{ height: 50, marginTop: 6, marginBottom: 14 }}
              style={{ backgroundColor: 'white' }}
              itemStyle={{ justifyContent: 'flex-start' }}
              dropDownStyle={{ backgroundColor: 'white' }}
              onChangeItem={(item) => setLocation(item.label)}
            />

            {isVisible ? (
              <DropDownPicker
                zIndex={4000}
                placeholder="Choose a skill"
                //retrieved the skills from the refugees database (from IpHome)
                items={global.skills}
                arrowColor="steelblue"
                containerStyle={{ height: 50, marginTop: 6, marginBottom: 14 }}
                style={{ backgroundColor: 'white' }}
                itemStyle={{ justifyContent: 'flex-start' }}
                dropDownStyle={{ backgroundColor: 'white' }}
                onChangeItem={(item) => setSkill(item.label)}
              />
            ) : (
              <DropDownPicker
                zIndex={4000}
                placeholder="Choose a skill"
                items={skills}
                arrowColor="steelblue"
                containerStyle={{ height: 50, marginTop: 6, marginBottom: 14 }}
                style={{ backgroundColor: 'white' }}
                itemStyle={{ justifyContent: 'flex-start' }}
                dropDownStyle={{ backgroundColor: 'white' }}
                onChangeItem={(item) => setSkill(item.label)}
                dropDownStyle={{
                  backgroundColor: '#00000000',
                  borderColor: '#00000000'
                }}
                style={{
                  backgroundColor: '#00000000',
                  borderColor: '#00000000'
                }}
                showArrow={false}
                labelStyle={{ fontSize: 0 }}
              />
            )}

            <Input
              onFocus={() => setUnderLineColor1('steelblue')}
              onBlur={() => setUnderLineColor1('grey')}
              inputContainerStyle={{ borderBottomColor: underLineColor1 }}
              label="Job Title"
              placeholder="Save the children"
              onChangeText={(value) => setJobTitle(value)}
            />
            <Input
              onFocus={() => setUnderLineColor2('steelblue')}
              onBlur={() => setUnderLineColor2('grey')}
              inputContainerStyle={{ borderBottomColor: underLineColor2 }}
              label="Salary Per Hour ($)"
              placeholder="25"
              keyboardType="numeric"
              onChangeText={(value) => setSalary(value)}
            />

            <TouchableHighlight
              underlayColor="#1A000000"
              onPress={showDatepicker}
            >
              <Input
                editable={false}
                label="Job Starting Date"
                placeholder={`${new Date(date)}`.substring(0, 15)}
                // onChangeText={value => setSalary(value)}
              />
            </TouchableHighlight>

            {show && (
              <DateTimePicker
                format="DD-MM-YYYY"
                value={new Date()}
                mode="date"
                display="spinner"
                onChange={onChange}
              />
            )}

            <Input
              onFocus={() => setUnderLineColor3('steelblue')}
              onBlur={() => setUnderLineColor3('grey')}
              inputContainerStyle={{ borderBottomColor: underLineColor3 }}
              label="Job Duration in days"
              placeholder="7"
              keyboardType="numeric"
              onChangeText={(value) => setDuration(value)}
            />
            <Text style={styles.additionalText}>Additional Details</Text>
            <TextInput
              style={styles.additionalTextInput}
              multiline={true}
              placeholder="Please mention any additinal details here, such as the job timings and etc"
              onChangeText={(value) => setDetails(value)}
            />

            <Button
              color="steelblue"
              mode="contained"
              style={{ marginVertical: 32 }}
              labelStyle={{ fontSize: 18, color: 'white' }}
              // should dismiss the current page or redirect to sth else

              onPress={() => {
                onPublish();
              }}
            >
              publish job
            </Button>
          </View>
          <ModalView
            titleText={''}
            bodyText={'Your job has been posted!'}
            modalVisible={modalVisible}
          />
        </>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    marginVertical: 20,
    height : screenHeight * 1.3,
    width : screenWidth
  },
  headerText: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 26,
    marginBottom: 16
  },
  additionalText: {
    fontWeight: 'bold',
    fontWeight: 'bold',
    marginHorizontal: 4,
    fontSize: 16,
    marginBottom: 4,
    color: 'lightslategray'
  },

  additionalTextInput: {
    textAlignVertical: 'top',
    height: 120,
    backgroundColor: 'white',
    fontSize: 16,
    padding: 8,

    borderWidth: 1,
    borderColor: 'lightgrey',
    borderRadius: 4
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: '#F194FF'
  },
  buttonClose: {
    backgroundColor: '#2196F3'
  },
  textStyle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center'
  }
});
