import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  View,
  ActivityIndicator,
  Dimensions,
} from "react-native";
import { SwipeListView } from "react-native-swipe-list-view";
import * as firebase from "firebase";
import "firebase/firestore";
import _ from "lodash";
const screenHeight = Math.round(Dimensions.get("window").height);
const screenWidth = Math.round(Dimensions.get("window").width);

//This screen displays a list of notifications to the IP
export default function IpNotifications({ navigation }) {
  //constants needed for implementation of screen
  const db = firebase.firestore();
  const [loading, setLoading] = useState(true);
  const [notifications, setNotifications] = useState([]);
  const user_id = global.user_id;

//close row
  const closeRow = (rowMap, rowKey) => {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow();
    }
  };
//delete row
  const deleteRow = (rowMap, rowKey) => {
    closeRow(rowMap, rowKey);
    const newData = [...notifications];
    const prevIndex = notifications.findIndex((item) => item.key === rowKey);
    newData.splice(prevIndex, 1);
    setNotifications(newData);
  };

  const onRowDidOpen = (rowKey) => {
   console.log("This row opened", rowKey);
  };

  const renderItem = (data) => (
    //when user clicks on notification navigate to applicant profile with below details
    <TouchableHighlight
      onPress={() => {
        var docRef = db.collection("refugees").doc(data.item.applicant_id);
        docRef.get().then((doc) => {
            if (doc.exists) {
              navigation.navigate("IpApplicantProfile", {
                job: data.item,
                job_notification_id: data.item.job_notification_id,
                applicant: doc.data(),
                applicant_id: doc.id
              });
            }
          })
          .catch((error) => {
            console.log("Error getting document:", error);
          });
      }}
      style={styles.rowFront}
      underlayColor={"ghostwhite"}
    >
      {
        //display notication title and job title 
      }
      <View>
        <Text style={{fontSize:19, paddingBottom: 30, paddingTop: 10, }}>
          {data.item.notification_title}: {data.item.job_title}
        </Text>
      </View>
    </TouchableHighlight>
  );

  const renderHiddenItem = (data, rowMap) => (
    <View style={styles.rowBack}>
      <TouchableOpacity
        style={[styles.backRightBtn, styles.backRightBtnRight]}
        onPress={() => {
          //if user deletes notification delete it from database
          deleteRow(rowMap, data.item.key);
          var jobsRef = db
            .collection("notifications")
            .doc(data.item.job_notification_id);

          var removeNotification = jobsRef.update({
            applicant_ids: firebase.firestore.FieldValue.arrayRemove(
              data.item.applicant_id
            ),
          });
        }}
      >
        <Text style={styles.backTextWhite}>Delete</Text>
      </TouchableOpacity>
    </View>
  );
  //This function fetches the notifications for the IP
  //It checks if a job has the same ip_id as that of the logged in user
  //if so, then user has notifications so add them to array notifications, 

  useEffect(() => {
    let i = 0;
    global.hasNotifications = false;

    const subscriber = firebase
      .firestore()
      .collection("notifications")
      .onSnapshot((querySnapshot) => {
        const notifications = [];
        querySnapshot.forEach((documentSnapshot) => {
          if (
            documentSnapshot.data().applicant_ids != undefined &&
            documentSnapshot.data().ip_id == user_id
          ) {
            documentSnapshot.data().applicant_ids.forEach(myFunction);

            function myFunction(item, index) {
              notifications.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id + "" + i,
                notification_title: "New applicant for job",
                applicant_id: item,
                job_notification_id: documentSnapshot.id,
                opened: false
              });
            }
          }
        });

        setNotifications(notifications);
        setLoading(false);
      });

    return () => subscriber();
  }, []);
  if (loading) {
    return <ActivityIndicator />;
  }
  //if notificaitons array does have notification inside of it then display them, else print no new notifications
  return notifications.length === 0 ? (
    <Text style={styles.emptyMsg}>No New Notifications</Text>
  ) : (
    <View style={{ fontSize: 40 }}>
      <SwipeListView
        data={notifications}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        renderHiddenItem={renderHiddenItem}
        leftOpenValue={0}
        rightOpenValue={-75}
        previewRowKey={"0"}
        previewOpenValue={-40}
        previewOpenDelay={3000}
        onRowDidOpen={onRowDidOpen}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: screenWidth,
    height: screenHeight
  },
  backTextWhite: {
    color: "white",
  },
  notification: {
    fontSize: 40,
  },
  rowFront: {
    alignItems: "center",
    backgroundColor: "white",
    borderBottomColor: "gainsboro",
    borderBottomWidth: 2,
    justifyContent: "center",
    height: 70,
  },
  rowBack: {
    alignItems: "center",
    backgroundColor: "ghostwhite",
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 10,
  },
  backRightBtn: {
    alignItems: "center",
    bottom: 0,
    justifyContent: "center",
    position: "absolute",
    top: 0,
    width: 75,
  },
  backRightBtnRight: {
    backgroundColor: "red",
    right: 0,
    borderRadius: 1,
  },

  emptyMsg: {
      flex: 1,
      textAlign: "center",
      textAlignVertical: "center",
      fontWeight: "bold",
      fontSize: 18
    },
});
