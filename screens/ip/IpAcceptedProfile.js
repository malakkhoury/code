//This screen shows refugee's profile who have been accepted to the job (from job details)

import React from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import { ListItem, Avatar, Rating } from 'react-native-elements'
import { Button } from 'react-native-paper';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { ScrollView } from 'react-native-gesture-handler';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

//profile image avatar
const data =
{
    profile_url: 'https://www.seekpng.com/png/full/143-1435868_headshot-silhouette-person-placeholder.png',

}

export default function IpAcceptedProfile({ route, navigation }) {
    //constants needed for implementation of screen
    const db = firebase.firestore();
    const user_id = global.user_id;
    const { applicant, applicant_id } = route.params;
    console.log(user_id);
    console.log(applicant_id)
    return (
       
        <View style={styles.container}>
            <ScrollView>
            <ListItem
                containerStyle={{ backgroundColor: 'snow' }} >
                
                <Avatar size={74} source={{ uri: data.profile_url }} />
             
                   { //list refugee details from database 
                    }   
           
                <ListItem.Content>
                    <ListItem.Title  style={{fontWeight: 'bold'}}>{applicant.name}</ListItem.Title>

                    <Rating
                        readonly
                        imageSize={20}
                        startingValue={Number(applicant.rating)}
                        style={{ paddingVertical: 8 }} />
                </ListItem.Content>
            {
                //messaging refugee 
            }
            </ListItem>
            <Button
                icon="email"
                color="black"
                mode="outlined"
                style={{ marginBottom: 8, marginHorizontal: 8 }}
                labelStyle={{ fontSize: 18, }}
                onPress={() => navigation.navigate("Chat", {
                    user_id: user_id,
                    recipient_id: applicant_id,
                })}>
                MESSAGE
                </Button>
            
            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{color:'steelblue', fontSize: 20}}>Age</ListItem.Title>
                    <ListItem.Subtitle>{applicant.age}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}   >
                <ListItem.Content>
                    <ListItem.Title style={{color:'steelblue', fontSize: 20}}>Gender</ListItem.Title>
                    <ListItem.Subtitle>{applicant.gender}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}   >
                <ListItem.Content>
                    <ListItem.Title style={{color:'steelblue', fontSize: 20}}>Nationality</ListItem.Title>
                    <ListItem.Subtitle>{applicant.nationality}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color:'steelblue', fontSize: 20}}>Skill</ListItem.Title>
                    <ListItem.Subtitle>{applicant.skill}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color:'steelblue', fontSize: 20}}>Location</ListItem.Title>
                    <ListItem.Subtitle>{applicant.location}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                onPres
                containerStyle={{ backgroundColor: 'snow' }}>
                <ListItem.Content>
                    <ListItem.Title style={{color:'steelblue', fontSize: 20}}>Summary</ListItem.Title>
                    <ListItem.Subtitle>{applicant.summary}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            </ScrollView>
            
        </View>  
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 8,
        paddingHorizontal: 2,
        width : screenWidth,
        height : screenHeight
    },

    requestEditText: {
        padding: 4,
        textAlign: 'center',
        color: 'deepskyblue',
        textDecorationLine: 'underline',
    },

    buttonsContainer: {
        flexDirection: 'row',
        marginTop: 8,
        paddingBottom: 8,
        justifyContent: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: '#bcbbc1',
    }
});

