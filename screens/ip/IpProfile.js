import React from 'react'
import { View, StyleSheet, Dimensions, Text } from 'react-native'
import { ListItem } from 'react-native-elements'

const screenHeight = Math.round(Dimensions.get('window').height);

//This Screen Shows Implementing Partner's Profile

export default function IpProfile({navigation}) {
    //constants needed for implementation of screen
    //global.user saved from login and used throughout app
    const user = global.user; 

    //view user profile 
    //get details from database user collection
    return (
        <View>
            <Text>
            </Text>
            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{ paddingBottom: 10, color: 'steelblue'}}>Registeration Number</ListItem.Title>
                    <ListItem.Subtitle style={{ fontSize: 20}} >{user.reg_id}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title  style={{ paddingBottom: 10, color: 'steelblue'}}>Name</ListItem.Title>
                    <ListItem.Subtitle style={{ fontSize: 20}}>{user.name}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{ paddingBottom: 10, color: 'steelblue'}}>Gender</ListItem.Title>
                    <ListItem.Subtitle style={{ fontSize: 20}}>{user.gender}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{ paddingBottom: 10, color: 'steelblue'}}>NGO Name</ListItem.Title>
                    <ListItem.Subtitle style={{ fontSize: 20}} >{user.ngo_name}</ListItem.Subtitle>

                </ListItem.Content>
            </ListItem>
        </View>
    )
}

const styles = StyleSheet.create({

    header: {
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: screenHeight*0.1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'steelblue'
       
      },
      headerTitle: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        paddingTop: 29
      }
    })