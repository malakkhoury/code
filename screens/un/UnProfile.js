import React from 'react'
import { View } from 'react-native'
import { ListItem } from 'react-native-elements'

//This screen shows the UN Profile

export default function UnProfile() {

    //global.user saved from login and used throughout app
    const user = global.user;

     //display user profile 
    //get details from database user collection
    return (
        <View>
            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{ paddingBottom: 10, color: 'steelblue'}}>Registeration Number</ListItem.Title>
                    <ListItem.Subtitle style={{ fontSize: 20}}>{user.reg_id}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{ paddingBottom: 10, color: 'steelblue'}}>Name</ListItem.Title>
                    <ListItem.Subtitle style={{ fontSize: 20}}>{user.name}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{ paddingBottom: 10, color: 'steelblue'}}>Gender</ListItem.Title>
                    <ListItem.Subtitle style={{ fontSize: 20}}>{user.gender}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>
            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{ paddingBottom: 10, color: 'steelblue'}}>Job Title</ListItem.Title>
                    <ListItem.Subtitle style={{ fontSize: 20}} >{user.job_title}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

        </View>
    )
}
