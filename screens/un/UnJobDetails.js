import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet ,Dimensions, LogBox} from 'react-native'
import { ListItem } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';
import { ScrollView } from 'react-native-gesture-handler';




const screenHeight = Math.round(Dimensions.get('window').height);


//this screen displays details of the job.

export default function UnJobDetails({ route, navigation }) {
    //constants needed for implementation
    const db = firebase.firestore();
    const { job } = route.params;
    const [workers, setWorkers] = useState([]);
    const [ipName, setIpName] = useState("");
    const [ngoName, setNgoName] = useState("");

    //key extractor for flat list unique key
    const keyExtractor = (item, index) => index.toString();

   
    //ignore scrollview warning
    useEffect(() => {
       LogBox.ignoreLogs(["VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead."]);
    }, [])
    

     //on render, when user clicks on, "on-board refugee" navigate to IpAcceptedProfile screen
    //this is to limit the amount of files created, it just shows the details of the refugee accepted to do the job
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
        onPress={() => {
            var docRef = db.collection("refugees").doc(item.key);
            docRef.get().then((doc) => {
                if (doc.exists) {

                    navigation.navigate( "IpAcceptedProfile",
                        {
                            job: job,
                            applicant_id: doc.id,
                            applicant: doc.data(),
                        }
                    )
                }
            }).catch((error) => {
                console.log("Error getting document:", error);
            });
        }
        }
        >

            <ListItem.Content>
                <ListItem.Title >{item.name}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem >
    );

    //get implementing partner name for job details
    useEffect(() => {
    const subscriber = db
        .collection('implementing_partners')
        .onSnapshot(querySnapshot => {
            querySnapshot.forEach(documentSnapshot => {
              //fetch their name from implementing partner's collection using their id
              //set ip name for job
                if (job.ip_id == documentSnapshot.id) {
                    setIpName(documentSnapshot.data().name);
                }
                //set NGO name to ngo name from collection
                if (job.ip_id == documentSnapshot.id){
                  setNgoName(documentSnapshot.data().ngo_name)
                }
            });
        });
    return () => subscriber();
}, []);
    //get workers (if there are any) for job details
    useEffect(() => {
        const subscriber = db
            .collection('refugees')
            .onSnapshot(querySnapshot => {
                const workers = [];
                querySnapshot.forEach(documentSnapshot => {
                    if (job.accepted_ids != undefined && job.accepted_ids.length > 0) {
                        if (job.accepted_ids.includes(documentSnapshot.id)) {
                            workers.push({
                                key: documentSnapshot.id,
                                name: documentSnapshot.data().name,
                            });
                        }
                    }
                });
                setWorkers(workers);
            });


        return () => subscriber();
    }, []);



    //display data for job details 
    return (

        <View style={styles.container}>
            <ScrollView>
            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{color:'steelblue', fontSize: 18}}>Status</ListItem.Title>
                    <ListItem.Subtitle>{workers.length==0? <Text style={{color: 'red'}}> Job has no employees </Text> :  <Text style={{color: 'green'}}>Job has employees</Text>}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }} >
            <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}} >Implementing partner NGO</ListItem.Title>
                <ListItem.Subtitle>{ngoName}</ListItem.Subtitle>
            </ListItem.Content>
        </ListItem>
        <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Implementing Partner Name</ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                    <ListItem.Subtitle style={{fontSize: 18}}>{ipName}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                    <ListItem.Title  style={{color: 'steelblue', fontSize: 18}}>Job Title </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle style={{ fontSize: 18}}>{job.job_title}</ListItem.Subtitle>
                     <ListItem.Subtitle></ListItem.Subtitle>
                    <ListItem.Subtitle sttyle={{fontSize: 16}}>{job.details}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Location </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle style={{ fontSize: 18}}>{job.location}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                    <ListItem.Content>
                    <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Salary </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle style={{ fontSize: 18}}>{`${job.salary} / Hour`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Skill </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle style={{ fontSize: 18}}>{job.skill}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>

            <ListItem
                bottomDivider
                containerStyle={{ backgroundColor: 'snow' }}  >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Job duration </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle style={{ fontSize: 18}}>{`${job.duration} days`}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>


            <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Starting date </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle style={{ fontSize: 18}}>{job.startingDate}</ListItem.Subtitle>
                </ListItem.Content>
                </ListItem>

                <ListItem bottomDivider
                containerStyle={{ backgroundColor: 'snow' }} >
                <ListItem.Content>
                <ListItem.Title style={{color: 'steelblue', fontSize: 18}}>Ending Date </ListItem.Title>
                    <ListItem.Subtitle></ListItem.Subtitle>
                     <ListItem.Subtitle style={{ fontSize: 18}}>{job.expiry_date}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem>


            
            <Text style={styles.applicantText}> On-board Refugees</Text>

            {workers.length == 0 ?
                <Text style={{paddingLeft: 12, paddingTop: 12, fontSize: 16}}>No refugees on-board</Text> :
            <FlatList
                style={{ marginBottom: 50 }}
                keyExtractor={keyExtractor}
                data={workers}
                renderItem={renderItem} 
                />
            }
</ScrollView>
      </View>


    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 8,
        paddingHorizontal: 2
    },

    requestEditText: {
        padding: 4,
        textAlign: 'center',
        color: 'deepskyblue',
        textDecorationLine: 'underline',
    },
    applicantText: {
        marginTop: 8,
        marginLeft: 4,
        padding: 4,
        fontSize: 18,
        color: 'steelblue'
    }
});

