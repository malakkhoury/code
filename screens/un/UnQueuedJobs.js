import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';


//this screen shows a list of queued jobs

export default function UnQueuedJobs({ navigation }) {
    //constants needed for implementation

    const db = firebase.firestore();
    const [loading, setLoading] = useState(true);
    //create array called jobs (to store jobs in)
    const [jobs, setJobs] = useState([]);

    //fetch from jobs collection in database, after render, calculate timings of jobs in database if the current time minus the starting time
    //of the job, is less than 0, then the job is still to come, so push it to jobs array to later display as queued job 
    useEffect(() => {
        const subscriber = firebase.firestore()
            .collection('jobs')
            .onSnapshot(querySnapshot => {
                const jobs = [];
                querySnapshot.forEach(documentSnapshot => {
                    let current_time = new Date().getTime();
                    let starting_time = documentSnapshot.data().date;
                    let difference = current_time - starting_time;

                    if (difference < 0) {
                        jobs.push({
                            ...documentSnapshot.data(),
                            key: documentSnapshot.id,
                        });
                    }
                });
                setJobs(jobs);
                setLoading(false);
            });

            return () => subscriber();
    }, []);
    //key extractor for flatlist  unique key
    const keyExtractor = (item, index) => index.toString();
    //on user pressing on certain job, navigate to UnJobDetails screen with job item
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            onPress={() =>
                navigation.navigate("UnJobDetails",
                {
                    job: item,
                }
            )}
        >

        {
            //display job title 
        }
            <ListItem.Content>
                <ListItem.Title>{item.job_title}</ListItem.Title>
                <ListItem.Subtitle>{item.details.toString().substring(0, 30) + "..."}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem>
    );

    //if there are any jobs being stored in jobs array then display them, else say "no jobs in queue"
    return (

        <View style={styles.container}>

            <Text style={styles.headerText}>QUEUED JOBS</Text>

            <Divider style={{ backgroundColor: 'deepskyblue' }} />
            {
                jobs.length > 0 ?
                    <FlatList
                        style={{ marginBottom: 50 }}
                        keyExtractor={keyExtractor}
                        data={jobs}
                        renderItem={renderItem} />

                    : <Text style={styles.emptyMsg}>No Jobs in queue</Text>
            }

        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    },
    emptyMsg: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
    }

});