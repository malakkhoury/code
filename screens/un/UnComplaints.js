import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';

//this screen shows the requests sent by refugees to edit their profiles

export default function UnComplaints({ navigation }) {
    //constants needed for implementation
    const db = firebase.firestore();
    const [complaints, setComplaints] = useState([]);

//after every render, fetch requests from complaints collection, and add to complaints array
    useEffect(() => {
        const subscriber = firebase.firestore()
            .collection('complaints')
            .onSnapshot(querySnapshot => {
                const complaints = [];
                querySnapshot.forEach(documentSnapshot => {
                    complaints.push({
                        ...documentSnapshot.data(),
                        key: documentSnapshot.id,
                    });
                });
                setComplaints(complaints);
            });
        return () => subscriber();
    }, []);

    //key extractor for flatlist unique key 
    const keyExtractor = (item, index) => index.toString();

    //when user clicks on a request, navigate to UnComplaintDetails screen
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            onPress={() => {
                var docRef = db.collection("refugees").doc(item.refugee_id);
                docRef.get().then((doc) => {
                    if (doc.exists) {
                        navigation.navigate("UnComplaintDetails",
                            {
                                refugee_id:doc.id,
                                refugee: doc.data(),
                                complaint: item,
                            }
                        )
                    }
                    
                }).catch((error) => {
                    console.log("Error getting document:", error);
                });

            }
            }
        >
            {
                //screen title
            }
            <ListItem.Content>
                <ListItem.Title>Profile Edit Request</ListItem.Title>
                <ListItem.Subtitle>{item.refugee_name}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />

        </ListItem>
    );

    //display
    return (

        <View style={styles.container}>
            <Text style={styles.headerText}>Profile Edit Requests</Text>
            <Divider style={{ backgroundColor: 'deepskyblue' }} />
            {
             complaints.length > 0 ?
            <FlatList
                style={{ marginBottom: 50 }}
                keyExtractor={keyExtractor}
                data={complaints}
                renderItem={renderItem} />
                :
                //if there are no requests: 
                    <View>
                <Text style={styles.emptyMsg}>No requests at the moment{"\n"} </Text>
                    </View>
            }
        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    },
    emptyMsg: {
        textAlign: 'center',
        fontSize: 18,
    },

});