import React, { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'

import * as firebase from 'firebase';
import 'firebase/firestore';

//this screen shows ongoing jobs 

export default function UnCurrentJobs({ navigation }) {
    //constants needed for implementation
    const db = firebase.firestore();
    const [loading, setLoading] = useState(true);
    const [jobs, setJobs] = useState([]);

    //after rendering fetch jobs that are currently ongoing 
    useEffect(() => {
        const subscriber = db
            .collection('jobs')
            .onSnapshot(querySnapshot => {
                //array to store ongoing jobs
                const jobs = [];
                querySnapshot.forEach(documentSnapshot => {
                    //Calculate the time difference from when job was created to job duration
                   //If difference is <= duration of job duration, then the job is displayed
                   //push jobs into jobs array
                    let current_time = new Date().getTime();
                    let starting_time = documentSnapshot.data().date;
                    let difference = current_time - starting_time;
                    let days_difference = Math.floor(difference / 1000 / 60 / 60 / 24);

                    if (days_difference >= 0 && days_difference <= documentSnapshot.data().duration) {
                        jobs.push({
                            ...documentSnapshot.data(),
                            key: documentSnapshot.id,
                        });
                    }

                });
                setJobs(jobs);
                setLoading(false);
            });

            return () => subscriber();
    }, []);
    //key extractor for flatlist unique key 
    const keyExtractor = (item, index) => index.toString();
    const renderItem = ({ item }) => (
        //when user clicks on any of the jobs, navigate to UnJobDetails, with specific job details, (item)
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            onPress={() =>
                navigation.navigate("UnJobDetails",
                    {
                        job: item,

                    }
                )}
        >
            {
                //display job titles
            }
            <ListItem.Content>
                <ListItem.Title>{item.job_title}</ListItem.Title>
                <ListItem.Subtitle>{item.details.toString().substring(0, 30) + "..."}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem>
    );

    return (

        //show jobs, but if jobs array is empty, then say no on going jobs
        <View style={styles.container}>

            <Text style={styles.headerText}>CURRENT JOBS</Text>

            <Divider style={{ backgroundColor: 'deepskyblue' }} />
            {
                jobs.length > 0 ?
                    <FlatList
                        style={{ marginBottom: 50 }}
                        keyExtractor={keyExtractor}
                        data={jobs}
                        renderItem={renderItem} />

                    : <Text style={styles.emptyMsg}>No Ongoing Jobs</Text>
            }

        </View>
    )
}
const styles = StyleSheet.create({

    container: {
        flex: 1,
    },
    headerText: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        marginVertical: 8,
    },

    emptyMsg: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
    }
});