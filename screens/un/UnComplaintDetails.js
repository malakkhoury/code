import React, { useState } from 'react';
import {
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView,
    Text,
    View,
    Dimensions
} from 'react-native';
import { Button } from 'react-native-paper';
import { Input } from 'react-native-elements';
import { ListItem } from 'react-native-elements'
import * as firebase from 'firebase';
import 'firebase/firestore';
import DropDownPicker from 'react-native-dropdown-picker';

//this screen shows the details of the profile edit request sent by refugee user
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);


export default function UnComplaintDetails({ route, navigation }) {
    //constants needed for implementation
    const db = firebase.firestore();

    const [name, setName] = useState("");
    const [age, setAge] = useState("");
    const [gender, setGender] = useState("");
    const [nationality, setNationality] = useState("");
    const [skill, setSkill] = useState("");
    const [location, setLocation] = useState("");
    const [isVisible, setIsVisible] = useState(true);

    const [underLineColor1, setUnderLineColor1] = useState('grey');
    const [underLineColor2, setUnderLineColor2] = useState('grey');
    const [underLineColor3, setUnderLineColor3] = useState('grey');
    const [underLineColor5, setUnderLineColor5] = useState('grey');
    const [underLineColor6, setUnderLineColor6] = useState('grey');

    const { refugee_id, refugee, complaint } = route.params;

    //function created for when UN user clicks on 'Resolve' request: 
    //if any of the values are left empty then leave them as they were in database
    //if not then they will be set as user changes them to
    //then alert user has request has been resolved
    const onResolve = () => {
        var docRef = db.collection("refugees").doc(refugee_id);
        return docRef.update({
            name: name == "" ? refugee.name : name,
            age: age == "" ? refugee.age : age,
            gender: gender == "" ? refugee.gender : gender,
            nationality: nationality == "" ? refugee.nationality : nationality,
            skill: skill == "" ? refugee.skill : skill,
            location: location == "" ? refugee.location : location,
        })
            .then(() => {
                var complaintsRef = db.collection('complaints').doc(complaint.key).delete();
                alert("This request has been resloved");
                navigation.goBack();
            })
            .catch((error) => {

                console.error("Error updating document: ", error);
            });

    }

    //function created for when UN user clicks on 'Dismiss'
    //delete request key, from complaints collection.
    const onDismiss = () => {
        var complaintsRef = db.collection('complaints').doc(complaint.key).delete();
        alert("This Complaint Has Been Dismissed");
        navigation.goBack();
    }

    //display data and update.
    return (
        <ScrollView
            keyboardShouldPersistTaps="handled"
            showsVerticalScrollIndicator={false}>
            <KeyboardAvoidingView enabled >

                <View style={styles.container}>

                    <ListItem
                        containerStyle={{ backgroundColor: 'white', elevation: 1, borderRadius: 4 }}  >
                        <ListItem.Content>
                            <Text style={styles.headerText}>Current Information</Text>
                            <ListItem.Subtitle></ListItem.Subtitle>
                            <ListItem.Subtitle style={{fontSize:18, fontWeight:'bold'}}>Name:   </ListItem.Subtitle><ListItem.Subtitle>{refugee.name}</ListItem.Subtitle>
                            <ListItem.Subtitle></ListItem.Subtitle>
                            <ListItem.Subtitle style={{fontSize:18, fontWeight:'bold'}}>Age   </ListItem.Subtitle><ListItem.Subtitle>{refugee.age}</ListItem.Subtitle>
                            <ListItem.Subtitle></ListItem.Subtitle>
                            <ListItem.Subtitle style={{fontSize:18, fontWeight:'bold'}}>Gender:   </ListItem.Subtitle><ListItem.Subtitle>{refugee.gender}</ListItem.Subtitle>
                            <ListItem.Subtitle></ListItem.Subtitle>
                            <ListItem.Subtitle style={{fontSize:18, fontWeight:'bold'}}>Nationality:   </ListItem.Subtitle><ListItem.Subtitle>{refugee.nationality}</ListItem.Subtitle>
                            <ListItem.Subtitle></ListItem.Subtitle>
                            <ListItem.Subtitle style={{fontSize:18, fontWeight:'bold'}}>Skill:   </ListItem.Subtitle><ListItem.Subtitle>{refugee.skill}</ListItem.Subtitle>
                            <ListItem.Subtitle></ListItem.Subtitle>
                            <ListItem.Subtitle style={{fontSize:18, fontWeight:'bold'}}>Location:   </ListItem.Subtitle><ListItem.Subtitle>{refugee.location}</ListItem.Subtitle>
                        </ListItem.Content>
                    </ListItem>

                    <ListItem
                        containerStyle={{ backgroundColor: 'white', elevation: 1, marginVertical: 8, borderRadius: 4 }}  >
                        <ListItem.Content>
                            <Text style={styles.headerText}>Requested Changes</Text>
                            <ListItem.Subtitle>{complaint.details}</ListItem.Subtitle>

                        </ListItem.Content>
                    </ListItem>
                    
               
                    <Input
                        onFocus={() => setUnderLineColor1('deepskyblue')}
                        onBlur={() => setUnderLineColor1('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor1 }}
                        label="Full Name"
                        onChangeText={value => setName(value)}
                    />
                    <Input
                        onFocus={() => setUnderLineColor2('deepskyblue')}
                        onBlur={() => setUnderLineColor2('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor2 }}
                        label="Age"
                        keyboardType="numeric"
                        onChangeText={value => setAge(value)}
                    />


                    <Input
                        onFocus={() => setUnderLineColor3('deepskyblue')}
                        onBlur={() => setUnderLineColor3('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor3 }}
                        label="Gender"
                        //placeholder="Male"
                        onChangeText={value => setGender(value)}
                    />

                    <Input
                        onFocus={() => setUnderLineColor5('deepskyblue')}
                        onBlur={() => setUnderLineColor5('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor5 }}
                        label="Nationality"
                       // placeholder="Syrian"
                        onChangeText={value => setNationality(value)}
                    />


                    <Input
                        onFocus={() => setUnderLineColor6('deepskyblue')}
                        onBlur={() => setUnderLineColor6('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor6 }}
                        label="Skill"
                        onChangeText={value => setSkill(value)}
                    />
                      <Input
                        onFocus={() => setUnderLineColor6('deepskyblue')}
                        onBlur={() => setUnderLineColor6('grey')}
                        inputContainerStyle={{ borderBottomColor: underLineColor6 }}
                        label="Location"
                        onChangeText={value => setLocation(value)}
                    />


                    <View style={{ flexDirection: 'row', marginTop: 16, justifyContent: 'center' }} >
                        <Button
                            icon="check"
                            color="deepskyblue"
                            mode="contained"
                            style={{ marginHorizontal: 16, width: screenWidth / 3 }}
                            labelStyle={{ fontSize: 18, color: "white" }}
                            onPress={onResolve}>
                            resolve
                        </Button>

                        <Button
                            icon="close"
                            color="red"
                            mode="contained"
                            style={{ marginHorizontal: 16, width: screenWidth / 3 }}
                            labelStyle={{ fontSize: 18, color: "white" }}
                            onPress={onDismiss}>
                            dismiss
                        </Button>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </ScrollView >

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 8,
        paddingTop: 8,
        paddingBottom: 32,
        height: screenHeight * 1.9
    },
    headerText: {
        fontSize: 18,
        marginBottom: 8,
        fontSize: 18,
        color: 'steelblue',
        fontWeight: 'bold'

    },
    choiceContainer: {
        backgroundColor: "white",
        elevation: 4,
        borderRadius: 8,
        marginBottom: 16,

    },
    userSelectionTile: {
        paddingLeft: 10,
        margin: 4,
    },

    infoText: {
        textAlign: 'center',
        marginHorizontal: 10,
        fontSize: 14,
        marginTop: 64,
    },
});