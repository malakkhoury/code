import React, {useEffect} from 'react';
import {Text, View, Stylesheet, Button} from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import * as firebase from 'firebase';
import 'firebase/firestore';

import UnProfileScreen from './un/UnProfile';
import UnPreviousJobsScreen from './un/UnPreviousJobs';
import UnCurrentJobsScreen from './un/UnCurrentJobs';
import UnQueuedJobsScreen from './un/UnQueuedJobs';
import UnComplaintsScreen from './un/UnComplaints';

//logout signout from firebase and navigate to landing page (login page)
function Logout({ navigation }) {
    useEffect(() => {
        firebase.auth().signOut().then(() => {
            navigation.replace("Landing");
        })
            .catch(error => alert(error.message))
    }, []);
    return (
        <View></View>
    );
}

const Drawer = createDrawerNavigator();
//drawers for naivgation.
export default function unHome() {
    return (
        <Drawer.Navigator initialRouteName="RfProfileScreen">
            <Drawer.Screen name="Profile" component={UnProfileScreen} />
            <Drawer.Screen name="All Current Jobs" component={UnCurrentJobsScreen} />
            <Drawer.Screen name="All Queued Jobs" component={UnQueuedJobsScreen} />
            <Drawer.Screen name="All Previous Jobs" component={UnPreviousJobsScreen} />
            <Drawer.Screen name="All Profile Edit Requests" component={UnComplaintsScreen} />
            <Drawer.Screen name="Logout" component ={Logout}/>
        </Drawer.Navigator>

    );
}