import * as React from "react";
import { useEffect } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Icon } from "react-native-elements";
import * as firebase from "firebase";
import "firebase/firestore";

import RfProfileScreen from "./rf/RfProfile";
import RfCurrentJobScreen from "./rf/RfCurrentJob";
import RfQueuedJobsScreen from "./rf/RfQueuedJobs";
import RfPreviousJobsScreen from "./rf/RfPreviousJobs";

//logout signout from firebase and navigate to landing page (login page)
function Logout({ navigation }) {
  useEffect(() => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        navigation.replace("Landing");
      })
      .catch((error) => alert(error.message));
  }, []);
  return <View></View>;
}

const Drawer = createDrawerNavigator();

export default function RfHome({ navigation }) {
  useEffect(() => {
    //check if user has notficaitons from database, if ids include the user id, then user has notificaiton
    const subscriber = firebase
      .firestore()
      .collection("notifications")
      .onSnapshot((querySnapshot) => {
        global.hasNotifications = false;
        querySnapshot.forEach((documentSnapshot) => {
          if (!!documentSnapshot.data()) {
            if (
              documentSnapshot.data().accepted_ids != undefined &&
              documentSnapshot.data().ids != undefined
            ) {
              if (
                documentSnapshot.data().accepted_ids.includes(user_id) ||
                documentSnapshot.data().ids.includes(user_id)
              ) {
                global.hasNotifications = true;
              }
                //messaging and notification icons and navigation
               //if user has notifications set bell-alert icon to red 
                navigation.setOptions({
                  headerRight: () => (
                    <View style={{ flexDirection: "row" }}>
                      <TouchableOpacity
                        style={{ marginRight: 8 }}
                        onPress={() => navigation.navigate("RfNotifications")}
                      >
                        {global.hasNotifications ? (
                          <Icon
                            type="material-community"
                            name="bell-alert"
                            size={32}
                            color="red"
                          />
                        ) : (
                          <Icon
                            type="material"
                            name="notifications"
                            size={32}
                            color="#D3D3D3"
                          />
                        )}
                      </TouchableOpacity>

                      <TouchableOpacity
                        style={{ marginRight: 8 }}
                        onPress={() => navigation.navigate("ChatList")}
                      >
                          <Icon
                            type="material"
                            name="email"
                            size={32}
                            color="#D3D3D3"
                          />
                      </TouchableOpacity>
                    </View>
                  ),
                });
                return;
              }
            }
        });
      });
    return () => subscriber();
  }, []);

  //drawers for naivgation.
  return (
    <Drawer.Navigator initialRouteName="RfProfileScreen">
      <Drawer.Screen name="Profile" component={RfProfileScreen} />

      <Drawer.Screen
        name="Queued Dates Jobs"
        component={RfQueuedJobsScreen}
        drawerStyle={{ width: 40 }}
      />
      <Drawer.Screen name="Current Dates Job" component={RfCurrentJobScreen} />
      <Drawer.Screen
        name="Previous Dates Jobs"
        component={RfPreviousJobsScreen}
      />
      <Drawer.Screen name="Logout" component={Logout} />
    </Drawer.Navigator>
  );
}
