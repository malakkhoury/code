//variables that are global as they need to be accessible through entire program.

//user object, IP or unhcr, refugee -> store the user object
global.user = {};
// -> store user_id number
global.user_id = ""; 
// -> store user type (refugee, implementing partner, or UNHCR)
global.user_type = "";

//store locations and skills saved for refugees in firebase, this is to later display job creation criterias of available refugees. 
global.locations = [];
global.skills = [];


