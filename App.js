//the main file from which the app is loaded.

//importing modules/features/screens to use
import React from 'react';
import { View, TouchableOpacity, LogBox, StyleSheet, Dimensions} from 'react-native';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Icon } from 'react-native-elements';
import { DrawerActions } from '@react-navigation/native';

const screenHeight = Math.round(Dimensions.get('window').height);

import * as firebase from 'firebase';
import 'firebase/firestore';
import './globals';

//importing screens 
import LandingScreen from './screens/auth/Landing';
import RegisterScreen from './screens/auth/Register';
import UnHomeScreen from './screens/UnHome';
import IpHome from './screens/IpHome';
import RfHomeScreen from './screens/RfHome';
import UnComplaintDetailsScreen from './screens/un/UnComplaintDetails';
import RfJobDetailsScreen from './screens/rf/RfJobDetails';
import RfSummaryScreen from './screens/rf/RfSummary';
import RfProfileEditScreen from './screens/rf/RfProfileEdit';
import RfCurrentJobScreen from './screens/rf/RfCurrentJob';
import RfNotificationsScreen from './screens/rf/RfNotifications';
import IpNotificationsScreen from './screens/ip/IpNotifications';
import IpCurrentJobDetailsScreen from './screens/ip/IpCurrentJobDetails';
import IpCreateJobScreen from './screens/ip/IpCreateJob';
import IpQueuedJobDetailsScreen from './screens/ip/IpQueuedJobDetails';
import IpAcceptedProfileScreen from './screens/ip/IpAcceptedProfile';
import IpRatingProfileScreen from './screens/ip/IpRatingProfile';
import IpApplicantProfileScreen from './screens/ip/IpApplicantProfile';
import RfQueuedJobDetailsScreen from './screens/rf/RfQueuedJobDetails';
import UnJobDetailsScreen from './screens/un/UnJobDetails';
import RfPreviousJobsDetailsScreen from './screens/rf/RfPreviousJobsDetails';
import ChatListScreen from './components/ChatList';
import ChatScreen from './components/Chat';
import ScanIdScreen from './components/ScanId';
import IpRatingJobs from './screens/ip/IpRatingJobs';
import UnRefugeeProfileScreen from './screens/un/UnRefugeeProfile';

//Initializing the firebase config, this information is required to connect the application to the firebase backend. (gotten from firebase console)
const firebaseConfig = {
  apiKey: 'AIzaSyCWzQ94sNCrm6Kgu4q-v64E7fW-tLYD7_M',
  authDomain: 'unhcr-99536.firebaseapp.com',
  projectId: 'unhcr-99536',
  storageBucket: 'unhcr-99536.appspot.com',
  messagingSenderId: '801884243451',
  appId: '1:801884243451:web:a458a4366f7551e2ee375e'
};

//initializing the app only if it still hasn't been initialized
if (firebase.apps.length == 0) {
  firebase.initializeApp(firebaseConfig);
  firebase.firestore().settings({ experimentalForceLongPolling: true, 
});
}
LogBox.ignoreLogs(['Setting a timer for a long period of time']);
firebase.firestore().settings({ experimentalForceLongPolling: true });

const Stack = createStackNavigator();
const navTheme = DefaultTheme;
navTheme.colors.background = 'snow';

//executing
export default function App() {
  return (
    // All the Screens in the app (stacks)
    //"Landing" is the name of the screen that the app will first land in
    <NavigationContainer theme={navTheme}>
      <Stack.Navigator
        initialRouteName="Landing"
        screenOptions={{
          headerStyle: {
            elevation: 1,
            shadowOpacity: 0
          }
        }}
      >
        <Stack.Screen
          name="Landing"
          component={LandingScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Register"
          component={RegisterScreen}
          options={{
            title: 'Create an Account',
            headerBackTitleVisible: false
          }}
        />
        <Stack.Screen
          name="UnHome"
          component={UnHomeScreen}
          options={({ navigation }) => ({
            title: 'UNHCR',
            headerStyle: {
              height: screenHeight*0.1,
              backgroundColor: 'steelblue'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              color:"#D3D3D3",
              fontSize: 20
  
            },
            headerLeft: () => (
              <TouchableOpacity
                onPress={() =>
                  navigation.dispatch(DrawerActions.toggleDrawer())
                }
              >
                <Icon type="material" name="menu" size={32} color="#D3D3D3" />
              </TouchableOpacity>
            ),
            headerLeftContainerStyle: { paddingLeft: 10 }
          })}
        />

        <Stack.Screen
          name="IpHome"
          component={IpHome}
          options={({ navigation }) => ({
            title: 'Implementing Partner',
            headerStyle: {
              height: screenHeight*0.1,
              backgroundColor: 'steelblue'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              color:"#D3D3D3",
              fontSize: 20
  
            },
            headerLeft: () => (
              <TouchableOpacity
                onPress={() =>
                  navigation.dispatch(DrawerActions.toggleDrawer())
                }
              >
                <Icon type="material" name="menu" size={32} color="#D3D3D3"/>
              </TouchableOpacity>
            ),
            headerRight: () => (
              <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                  style={{ marginRight: 8 }}
                  onPress={() => navigation.navigate('IpNotifications')}
                >
                  <Icon type="material" name="notifications" size={32} color="#D3D3D3"/>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{ marginRight: 8 }}
                  onPress={() => navigation.navigate('ChatList')}
                >
                  <Icon type="material" name="email" size={32} color="#D3D3D3" />
                </TouchableOpacity>
              </View>
            ),

            headerLeftContainerStyle: { paddingLeft: 10 }
          })}
        />

        <Stack.Screen
          name="RfHome"
          component={RfHomeScreen}
          options={({ navigation }) => ({
            title: 'Refugee',
            headerStyle: {
              height: screenHeight*0.1,
              backgroundColor: 'steelblue'
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              color:"#D3D3D3",
              fontSize: 20
  
            },
            headerLeft: () => (
              <TouchableOpacity
                onPress={() =>
                  navigation.dispatch(DrawerActions.toggleDrawer())
                }
              >
                <Icon type="material" name="menu" size={32} color="#D3D3D3" />
              </TouchableOpacity>
            ),
            headerRight: () => (
              <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                  style={{ marginRight: 8 }}
                  onPress={() => navigation.navigate('RfNotifications')}
                >
                  <Icon type="material" name="notifications" size={32} color="#D3D3D3" />
                </TouchableOpacity>

                <TouchableOpacity
                  style={{ marginRight: 8 }}
                  onPress={() => navigation.navigate('ChatList')}
                >
                  <Icon type="material" name="email" size={32} color="#D3D3D3" />
                </TouchableOpacity>
              </View>
            ),

            headerLeftContainerStyle: { paddingLeft: 10 }
          })}
        />
         <Stack.Screen
          name="RfPreviousJobsDetails"
          component={RfPreviousJobsDetailsScreen}
          options={{ title: 'JOB DETAILS'}}

        />
        <Stack.Screen
          name="UnComplaintDetails"
          component={UnComplaintDetailsScreen}
          options={{ title: 'Edit Refugee Profile' }}
        />
        <Stack.Screen
          name="RfJobDetails"
          component={RfJobDetailsScreen}
          options={{ title: 'JOB DETAILS' }}
        />
        <Stack.Screen
          name="RfSummary"
          component={RfSummaryScreen}
          options={{ title: 'Edit Summary' }}
        />
        <Stack.Screen
          name="RfProfileEdit"
          component={RfProfileEditScreen}
          options={{ title: 'Edit Profile Info' }}
        />
        <Stack.Screen
          name="RfCurrentJob"
          component={RfCurrentJobScreen}
          options={{ title: 'Current Recruitment' }}
        />
        <Stack.Screen
          name="RfNotifications"
          component={RfNotificationsScreen}
          options={{ title: 'Notifications' }}
        />
        <Stack.Screen
          name="IpCreateJob"
          component={IpCreateJobScreen}
          options={{ title: 'Create Job' }}
        />
        <Stack.Screen
          name="IpCurrentJobDetails"
          component={IpCurrentJobDetailsScreen}
          options={{ title: 'JOB DETAILS' }}
        />
        <Stack.Screen
        name="IpRatingJobs"
        component={IpRatingJobs}
        options={{title: 'JOB TO BE RATED'}}
        />
        <Stack.Screen
          name="ScanId"
          component={ScanIdScreen}
          options={{ title: 'Scan ID', headerBackTitleVisible: false }}
        />
        <Stack.Screen
          name="IpApplicantProfile"
          component={IpApplicantProfileScreen}
          options={{ title: 'Applicant Details' }}
        />
        <Stack.Screen
          name="IpAcceptedProfile"
          component={IpAcceptedProfileScreen}
          options={{ title: 'Refugee Details' }}
        />
        <Stack.Screen
          name="IpRatingProfile"
          component={IpRatingProfileScreen}
          options={{ title: 'Refugee Rating' }}
        />
        <Stack.Screen
          name="ChatList"
          component={ChatListScreen}
          options={{ title: 'Chat List' }}
        />
        <Stack.Screen
          name="Chat"
          component={ChatScreen}
          options={{ title: 'Chat'}}
        />
        <Stack.Screen
          name="IpNotifications"
          component={IpNotificationsScreen}
          options={{ title: 'Notifications' }}
        />
        <Stack.Screen
          name="IpQueuedJobDetails"
          component={IpQueuedJobDetailsScreen}
          options={{ title: 'JOB DETAILS' }}
        />
        <Stack.Screen
          name="RfQueuedJobDetails"
          component={RfQueuedJobDetailsScreen}
          options={{ title: 'JOB DETAILS' }}
        />
        <Stack.Screen
          name="UnJobDetails"
          component={UnJobDetailsScreen}
          options={{ title: 'JOB DETAILS' }}
        />
        <Stack.Screen
          name="UnRefugeeProfile"
          component={UnRefugeeProfileScreen}
          options={{ title: 'Refugee Details' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
