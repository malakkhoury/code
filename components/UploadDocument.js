//not working properly, will work on in the future. code idea gotten from : https://stackoverflow.com/questions/66967900/expo-document-picker-is-not-displaying-to-upload-a-file
import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity  } from 'react-native';
import * as DocumentPicker from 'expo-document-picker';
import { Icon } from 'react-native-elements'

const UploadFile = () =>{
    const pickDocument = async () => {
        let result = await DocumentPicker.getDocumentAsync({});
    }

    
    return(
        <View style={styles.background} >
            <View style={styles.button} >
            <TouchableOpacity
                    activeOpacity={0.7}
                    underlayColor="#cccccc"
                    style={{ borderRadius: 2 }}
                    onPress={pickDocument}>
                 <Text style={{ fontSize: 16, color: 'deepskyblue', textAlign: 'center' }}>Please attach a legal document verifying the data you have provided.</Text>
                 <Icon type="material" name="attachment" color="deepskyblue" size={42} />
            </TouchableOpacity>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    background:{
        backgroundColor:"white",  
    },
    file:{
        color:'black',
        marginHorizontal:145,
    },
    button:{
        marginHorizontal:60,
    }
});

export default UploadFile