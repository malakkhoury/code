// chat screen
import React, { useState, useEffect } from "react";
import { KeyboardAvoidingView } from "react-native";
import {
  TouchableOpacity,
  TextInput,
  FlatList,
  View,
  Dimensions,
  StyleSheet,
  Text,
} from "react-native";
import { Icon } from "react-native-elements";
import * as firebase from "firebase";
import "firebase/firestore";

//This screen renders the chat between 2 users
//It fetches the recipient using the recipient_id from the ChatList screen
//and then fetches the messages under that recipient_ID

export default function RfChatWithIp({ route }) {
  //constants needed for implementation of screen
  const db = firebase.firestore();
  const { user_id, recipient_id } = route.params;
  const [textMessage, setTextMessage] = useState("");
  const [messageList, setMessageList] = useState([]);

  //"useEffect runs both after the first render and after every update"
  //Get messages from firebase when screen is rendered
  useEffect(() => {
    //This fetches data from firebase by going to Chats->{User_ID}->Recipients->{Recipient_ID}->Messages
    const subscriber = firebase
      .firestore()
      .collection("chats")
      .doc(user_id)
      .collection("recipients")
      .doc(recipient_id)
      .collection("messages")
      .orderBy("time", "asc")
      .onSnapshot((querySnapshot) => {
        const messages = [];
        querySnapshot.forEach((documentSnapshot) => {
          messages.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
          });
        });
        setMessageList(messages);
      });
      console.log(messageList);
    // Unsubscribe from events when no longer in use
    return () => subscriber();
  }, []);

  //This function displays time on messages in chat
  const convertTime = (time) => {
    //Creates a new JavaScript Date object based on the timestamp
    var date = new Date(time);
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();
    var formattedTime = hours + ":" + minutes.substr(-2);
    return formattedTime;
  };
  
  //This function allows user to send message by setting the values to dummy fields in the chats collection:
  //Dummy fields hold data temporarily (in memory) for processing operations, display, and printing.
  // after that the message is added to the database storing the user id and the recipent id
  const sendMessage = async () => {
    if (textMessage.length > 0) {
      var docRef = db
        .collection("chats")
        .doc(user_id)
        .set(
          {
            dummyField: "dummyField",
          },
          { merge: true }
        )
        .catch((error) => {
          console.log("Error getting document:", error);
        });
      var docRef = db
        .collection("chats")
        .doc(recipient_id)
        .set(
          {
            dummyField: "dummyField",
          },
          { merge: true }
        )
        .catch((error) => {
          console.log("Error getting document:", error);
        });
      var docRef = db
        .collection("chats")
        .doc(user_id)
        .collection("recipients")
        .doc(recipient_id)
        .set(
          {
            dummyField: "dummyField",
          },
          { merge: true }
        )
        .catch((error) => {
          console.log("Error getting document:", error);
        });
      var docRef = db
        .collection("chats")
        .doc(recipient_id)
        .collection("recipients")
        .doc(user_id)
        .set(
          {
            dummyField: "dummyField",
          },
          { merge: true }
        )
        .catch((error) => {
          console.log("Error getting document:", error);
        });

      //Add message to database of the current user
      db.collection("chats")
        .doc(user_id)
        .collection("recipients")
        .doc(recipient_id)
        .collection("messages")
        .add({
          message: textMessage,
          time: new Date().getTime(),
          from: user_id,
        })
        .then((docRef) => {
          console.log("Document written with ID: ", docRef.id);
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });
      // Add message to database of current recipient
      db.collection("chats")
        .doc(recipient_id)
        .collection("recipients")
        .doc(user_id)
        .collection("messages")
        .add({
          message: textMessage,
          time: new Date().getTime(),
          from: user_id,
        })
        .then((docRef) => {
          console.log("Document written with ID: ", docRef.id);
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });

      setTextMessage("");
    }
  };

  //preview of chats
  const renderRow = ({ item }) => {
    return (
      <View
        style={{
          alignContent: "space-around",
          width: "60%",

          alignSelf: item.from === user_id ? "flex-end" : "flex-start",
          backgroundColor:
            item.from === global.user_id ? "deepskyblue" : "grey",
          borderRadius: 6,
          marginBottom: 10,
        }}
        
      >
        <Text
          style={{
            color: "#fff",
            paddingHorizontal: 8,
            paddingTop: 4,
            fontSize: 16,
          }}
        >   
          {item.message}
        </Text>
        <Text
          style={{
            color: "gainsboro",
            paddingHorizontal: 6,
            paddingBottom: 4,
            fontSize: 12,
            alignSelf: "flex-end",
          }}
        >
          {convertTime(item.time)}
        </Text>
      </View>
    );
  };

  let { height, width } = Dimensions.get("window");
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <View style={styles.container}>
        <FlatList
          style={{ padding: 10, height: height * 0.8 }}
          data={messageList}
          renderItem={renderRow}
          //keyExtractor for flatlist unique keys
          keyExtractor={(item, index) => index.toString()}
        />
        <View style={styles.inputBox}>
          <TextInput
            fontSize={18}
            style={styles.inputText}
            value={textMessage}
            placeholder="Type a message..."
            onChangeText={(value) => setTextMessage(value)}
            keyboardType={"default"}
            autoCorrect={false}
          />
          <TouchableOpacity
            onPress={sendMessage}
            style={{
              margin: 1,
              padding: 10,
              borderRadius: 100,
              elevation: 0.4,
              backgroundColor: "#d7e0e3",
            }}
          >
            <Icon color="deepskyblue" type="material" name="send" size={32} />
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputText: {
    padding: 10,
    width: "80%",
    marginLeft: 10,
  },
  inputBox: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#d7e0e3",
    borderWidth: 1,
    borderColor: "#ccc",
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    marginBottom: 80,
  },
  btnText: {
    color: "darkblue",
    fontSize: 20,
  },
});