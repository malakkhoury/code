import React, { useRef, useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList
} from 'react-native';
import { ListItem, Divider } from 'react-native-elements'

import * as firebase from 'firebase';
import 'firebase/firestore';

//This screens renders a list of users you have ongoing chats with
//by fetching all recipients under the current user_id
export default function ChatList({ navigation }) {

    const [users, setUsers] = useState([]);
    const db = firebase.firestore();
    const user_id = global.user_id;
    const user_type = global.user_type == "implementing_partners" ? "refugees" : "implementing_partners";
  //This function gets those users with which the current user has any chats 
  useEffect(() => {
  //Fetches data from firebase through Chats->{User_ID}->Recipients
        const subscriber = firebase.firestore()
            .collection('chats')
            .doc(user_id)
            .collection("recipients")
            .onSnapshot
            (querySnapshot => {
                const users2 = [];
                querySnapshot.forEach(documentSnapshot => {
                //gets recipient based on  Recipient ID
                    var docRef = db.collection(user_type).doc(documentSnapshot.id);
                    docRef.get().then((doc) => {
                //if user has messages 
                        if (doc.exists) {
                            users2.push({
                                ...doc.data(),
                                key: doc.id,
                            });
                            setUsers(users2);
                        } else {
                            console.log("No such document!");
                        }
                    }).catch((error) => {
                        console.log("Error getting document:", error);
                    });
                });
            });
        return () => subscriber();
    }, []);

//preview, navigate to chats
    const renderItem = ({ item }) => (
        <ListItem
            bottomDivider
            containerStyle={{ backgroundColor: 'snow' }}
            //when clicked on the recipent navigate to the chat 
            onPress={() =>
                navigation.navigate("Chat",
                    {
                        user_id: user_id,
                        recipient_id: item.key
                    }
                )
            }
        >
            <ListItem.Content>
                <ListItem.Title>{item.name}</ListItem.Title>

            </ListItem.Content>
            <ListItem.Chevron color="deepskyblue" />
        </ListItem>
    );

    return (
        //no chats for user?
        <View style={styles.container}>
            {users.length == 0 ?
                <Text style={styles.emptyMsg}>No chats to show</Text> :
                //else
                <FlatList
                    data={users}
                    renderItem={renderItem}
                    //keyExtracytor for flatlist unique keys
                    keyExtractor={(item) => item.key}
                />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    emptyMsg: {
        flex: 1,
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 18,
    }
})
   